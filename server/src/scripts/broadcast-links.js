import mongoose from 'mongoose';
import mongooseConf from '../config/mongoose.conf.js';
import firebase from 'firebase-admin';
import firebaseConf from '../config/firebase.conf.js';
import Question from '../app/models/question.model.js';
import Broadcast from '../app/models/broadcast.model.js';
import {logger} from '../app/helper/logger.js';
import youtubeHelper from '../app/helper/youtube.js';
import moment from 'moment-timezone';
import dotenv from 'dotenv';
dotenv.config({ path: process.cwd() + '/../.env' });

firebaseConf(firebase);
mongooseConf(mongoose);

(async () => {
  const untaggedQuestions = await Question
    .find({
      answered: true,
      answeredAt: { $exists: true },
      broadcast: { $exists: false }
    })
    .exec();

  const ytHelper = youtubeHelper();

  // first try to tag with broadcasts from database
  for (const question of untaggedQuestions) {
    // some question have corrupt `answeredAt` field
    if (question.answeredAt < question.createdAt) {
      continue;
    }

    const broadcast = await Broadcast.findOne({
      started: { $lte: question.answeredAt },
      ended: { $gte: question.answeredAt }
    });

    if (broadcast) {
      logger.info('BROADCAST LINKS', `Added broadcast ${broadcast._id} to question ${question._id}`);
      question.broadcast = broadcast;
      await question.save();
    } else {
      const ytMatch = await ytHelper
        .findBroadcastByTime(
          moment.utc(question.answeredAt).toISOString(),
          moment.utc(question.answeredAt).add('18', 'hours').toISOString()
        );

      if (ytMatch && ytMatch.items && ytMatch.items.length > 0) {
        const liveStreamDetails = await ytHelper
          .getLiveStreamDetails(ytMatch.items[0].id.videoId);

        if (liveStreamDetails && liveStreamDetails.items && liveStreamDetails.items.length > 0) {
          logger.info('BROADCAST LINKS', `Discovered new broadcast link https://youtu.be/${ytMatch.items[0].id.videoId}`);

          const newBroadcast = await Broadcast.findOneAndUpdate({
            videoUrl: new String('https://youtu.be/' + ytMatch.items[0].id.videoId)
          }, {
            started: liveStreamDetails.items[0].liveStreamingDetails.actualStartTime,
            ended: liveStreamDetails.items[0].liveStreamingDetails.actualEndTime
          }, {
            new: true,
            upsert: true,
            setDefaultsOnInsert: true
          });

          question.broadcast = newBroadcast;
          await question.save();

          logger.info('BROADCAST LINKS', `Added broadcast ${newBroadcast._id} to question ${question._id}`);
        }
      } else {
        logger.info('BROADCAST LINKS', `Nothing found for question ${question._id}`);

        if (moment.utc().diff(question.answeredAt, 'days') > 3) {
          question.broadcast = null;
          await question.save();
        }
      }
    }
  }

  process.exit(0);
})().catch(err => {
  logger.error('BROADCAST LINKS', err);
  process.exit(1);
});

