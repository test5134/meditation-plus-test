import mongoose from 'mongoose';
import mongooseConf from '../config/mongoose.conf.js';
import firebase from 'firebase-admin';
import firebaseConf from '../config/firebase.conf.js';
import PushSubscriptions from '../app/models/push.model.js';
import {logger} from '../app/helper/logger.js';
import dotenv from 'dotenv';

dotenv.config({ path: process.cwd() + '/../.env' });
firebaseConf(firebase);
mongooseConf(mongoose);

logger.info('Cleaning up old Push-Subscriptions');

// Cleanup old (7 days) subscriptions
PushSubscriptions
  .find({
    updatedAt: { $lt: Date.now() - 6048e5 }
  })
  .deleteOne()
  .exec()
  .then(() => mongoose.connection.close());
