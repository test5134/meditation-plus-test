import {readFileSync} from 'fs';
import {logger}  from '../app/helper/logger.js';
import mongooseConf from '../config/mongoose.conf.js';
import firebase from 'firebase-admin';
import firebaseConf from '../config/firebase.conf.js';
import mongoose from 'mongoose';
import _ from 'lodash-es';
import dotenv from 'dotenv';

/**
 * convert modelName parameter to a class definition
 * @param modelName
 * @returns User.model, Question.model, etc
 */
async function getModel(modelName) {
  let model = await import(`../app/models/${modelName}.model.js`);
  if (!model)
    throw new Error(`file ../app/models/${modelName}.model.js not found`);

  if (Object.keys(model).includes('default')) {
    model = model.default;
  } else {
    model = model[Object.keys(model)[0]];
  }

  if (!model)
    throw new Error(`file definition ../app/models/${modelName}.model.js not found`);
  return model;
}

async function saveRow(klass, data) {
  // find duplicate
  let cleanData = _.omit(data, _.keys(mapping));
  let existing = await klass.findOne(cleanData);

  if (!existing) {
    let findResult = {};
    let mappedData = _.pick(data, _.keys(mapping));
    let keys = _.keys(mappedData);
    let findErr = false;

    for (let k of keys) {
      let res = await mapping[k](data[k]);

      if (res) {
        //collect findResult
        findResult = Object.assign(findResult, res);
      } else {
        findErr = true;
        logger.error(`mapping error: ${k} not found`, data[k]);
        break;
      }
    }

    if (!findErr) {
      // merge custom mapped field back to data
      let newData = Object.assign(cleanData, findResult);
      let row = new klass(newData);
      await row.save();
      logger.info('saving row', data);
    }

  } else {
    logger.error('duplicate row', data);
  }
}

async function parse_json(tableName, content) {
  // json content file has to be an array
  const fileContent = JSON.parse(content);
  let model = await getModel(tableName);
  for (let row of fileContent) {
    try {
      await saveRow(model, row);
    } catch (err) {
      logger.error('save exception ' + err.message, row);
    }
  }
}

async function readJson(t, filepath) {
  try {
    logger.info('start file ', filepath);
    let content = readFileSync(filepath);
    await parse_json(t, content);
    logger.info('file processing finished', filepath, '\n\n');
  } catch (err) {
    logger.error('readJson error ' + err);
  }
}

function init() {
  dotenv.config({ path: process.cwd() + '/../.env' });
  firebaseConf(firebase);
  mongooseConf(mongoose);
}

/**
 * convert map-user to user.id
 */
let userMapper = async (data) => {
  let model = await getModel('user');
  let user = await model.findOne(data);
  if (user) {
    return {user: user._id};
  } else {
    return null;
  }
};

/**
 * convert map-user to user.id
 */
let teacherMapper = async (data) => {
  let model = await getModel('user');
  let user = await model.findOne(data);
  if (user) {
    return {teacher: user._id};
  } else {
    return null;
  }
};

/**
 * convert map-local to local by hashing local.password
 */
let userLocalMapper = async (data) => {
  let ret = data;
  let model = await getModel('user');

  ret.password = new model().generateHash(ret.password);
  return {local: ret};
};

/**
 * mapping dictionary that map a field in the dev-data jsons
 */
const mapping = {
  'map-user': userMapper,
  'map-teacher': teacherMapper,
  'map-local': userLocalMapper
};

/**
 * @param tables are file name in dev-data folder that will be imported to the database
 * @returns {Promise.<void>}
 */
export async function start(tables) {
  init();
  for (let t of tables) {
    await readJson(t, `${process.cwd()}/src/scripts/dev-data/${t}.json`);
  }
  logger.info('after all processing');
  mongoose.connection.close();
}
