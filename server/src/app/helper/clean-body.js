import sanitize from 'mongo-sanitize';

function clean(object) {
  return sanitize(object);
}

export function cleanBody(req, res, next) {
  req.body = clean(req.body);
  next();
}

export function cleanParams(req, res, next) {
  req.params = clean(req.params);
  next();
}
