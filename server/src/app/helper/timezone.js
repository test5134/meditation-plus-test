import moment from 'moment-timezone';
import _ from 'lodash-es';

/**
* Method for converting times to user's timezone
*/
export default (user, time = undefined) => {
  const timezone = _.get(user, 'timezone', null);

  if (!timezone || !moment.tz.zone(timezone)) {
    return moment(time).utc();
  }

  return moment(time).tz(timezone);
};
