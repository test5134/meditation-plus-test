import { validateSchemaPartial } from './validate.js';
import mongoose from 'mongoose';
import validator from 'validator';
import chai from 'chai';
const { expect } = chai;

const UserTest = mongoose.model('UserTest', mongoose.Schema({
  local: {
    email: { type: String, validate: validator.isEmail }
  },
  username: {
    type: String,
    unique: true,
    index: true,
    sparse: true, // allow null
    validate: /^[a-zA-Z][a-zA-Z0-9-_.]{3,20}$/
  },
  name: String,
  personalNumber: { type: Number }
}));

const expectPromiseThrows = async (promise, error) => {
  try {
    await promise;
    expect(false).to.equal(true);
  } catch (err) {
    expect(err).to.be.instanceOf(error);
  }
};

describe('Validation Helper', () => {
  it('should ignore additional keys', async () => {
    expect(await validateSchemaPartial({ name: 123456, unknown: 123 }, UserTest)).to.eql({ name: '123456' });
    expect(await validateSchemaPartial({ name: 123456, local: { test: 'asd'} }, UserTest)).to.eql({ name: '123456' });
    expect(await validateSchemaPartial({ name: 123456, __proto__: { test: 123 } }, UserTest)).to.eql({ name: '123456' });
    expect(await validateSchemaPartial({ name: 123456, constructor: { test: 123 } }, UserTest)).to.eql({ name: '123456' });
    expect(await validateSchemaPartial({ constructor: { test: 123 } }, UserTest)).to.eql({});
  });

  it('should throw for invalid values', async () => {
    await expectPromiseThrows(validateSchemaPartial({ username: { test: 'ASD'} }, UserTest), mongoose.Error.ValidationError);
    await expectPromiseThrows(validateSchemaPartial({ username: 123 }, UserTest), mongoose.Error.ValidationError);
    await expectPromiseThrows(validateSchemaPartial({
      username: 'asd123',
      local: { email: 'asdasdasd at afdsf'}
    }, UserTest), mongoose.Error.ValidationError);
    await expectPromiseThrows(validateSchemaPartial({
      username: 'asd123',
      local: { email: 'test@test.de'},
      personalNumber: 'FIVE'
    }, UserTest), mongoose.Error.ValidationError);
  });

  it('should cast values', async () => {
    expect(await validateSchemaPartial({ name: 123456 }, UserTest)).to.eql({ name: '123456' });
    expect(await validateSchemaPartial({ personalNumber: '123456' }, UserTest)).to.eql({ personalNumber: 123456 });
    expect(await validateSchemaPartial({ personalNumber: '123456' }, UserTest)).to.eql({ personalNumber: 123456 });
  });

  it('should pass for valid values', async () => {
    expect(await validateSchemaPartial({ local: { email: 'test@test.de' } }, UserTest)).to.eql({ local: { email: 'test@test.de' } });
    expect(await validateSchemaPartial({ username: 'bobby' }, UserTest)).to.eql({ username: 'bobby' });
    expect(await validateSchemaPartial({
      local: { email: 'test@test.de' },
      username: 'testuser',
      name: 'Fred',
      personalNumber: 456
    }, UserTest)).to.eql({
      local: { email: 'test@test.de' },
      username: 'testuser',
      name: 'Fred',
      personalNumber: 456
    });
  });
});
