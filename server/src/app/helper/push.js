import { User } from '../models/user.model.js';
import FCMSubscription from '../models/fcm.model.js';
import firebase from 'firebase-admin';
import { logger } from './logger.js';

const push = {
  /**
   * Send a Push message out to a user.
   *
   * @param  username   either username or query to find user(s)
   * @param  data       payload for notification
   *
  * @returns True, if the message was successfully send to at
   *          least one device. False otherwise.
   */
  send: async (userquery, message) => {
    if (!userquery || !message) {
      return false;
    }


    const users = await User.find(userquery);

    if (!users) {
      return false;
    }

    const subscriptions = await FCMSubscription.find({
      user: { $in: users.map(u => u._id) }
    });

    if (!subscriptions) {
      return false;
    }

    let success = 0;
    for (const { token } of subscriptions) {
      try {
        await firebase.messaging().send({
          ...message,
          token
        });
        success++;
      } catch (error) {
        logger.error('FCM SENDING ERROR:', error);
      }
    }

    logger.info(`FCM SENT MESSAGE TO ${success} DEVICES`);

    return success > 0;
  },

  /**
   * Sends notification to all devices, which
   * subscribed to a specific topic.
   *
   * @param topic the topic name
   *
   * @returns True if no errors happened. False, otherwise.
   */
  sendToTopic: async (topic, message) => {
    if (!topic) {
      return;
    }

    let success = true;
    try {
      await firebase.messaging().send({
        ...message,
        topic
      });
    } catch (error) {
      success = false;
      logger.error('FCM SENDING ERROR:', error);
    }

    return success;
  }
};

export default push;

