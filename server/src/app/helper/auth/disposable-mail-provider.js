import { readFileSync, existsSync } from 'fs';
import { logger } from '../logger.js';
const FILE = process.cwd() + '/src/config/disposable-provider.json';

/**
 * Check if email domain is a disposable email provider.
 *
 * @param {string} email
 * @returns {boolean}
 */
export function isDisposableEmailProvider(email) {
  if (!existsSync(FILE)) {
    return false;
  }

  const content = readFileSync(FILE);

  try {
    const json = JSON.parse(content);
    const parts = email.split('@');
    return parts.length === 2 && json.includes(parts[1]);
  } catch(e) {
    logger.error(e);
  }

  return false;
}
