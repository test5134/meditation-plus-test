import { readFileSync, existsSync } from 'fs';
import { logger } from '../logger.js';
const FILE_WHOLE_DOMAINS = process.cwd() + '/src/config/banned-hosts-whole.txt';
const FILE_PARTIAL_DOMAINS = process.cwd() + '/src/config/banned-hosts-partial.txt';

/**
 * Check if email domain is a spam email provider.
 *
 * @param {string} email
 * @returns {boolean}
 */
export function isSpamEmailProvider(email) {
  return checkWhole(email) || checkPartial(email);
}

function checkWhole(email) {
  if (!existsSync(FILE_WHOLE_DOMAINS)) {
    return false;
  }

  const domains = readFileSync(FILE_WHOLE_DOMAINS).toString().split('\n');

  try {
    const parts = email.split('@');
    return parts.length === 2 && domains.includes(parts[1].trim());
  } catch(e) {
    logger.error(e);
  }

  return false;
}

function checkPartial(email) {
  if (!existsSync(FILE_PARTIAL_DOMAINS)) {
    return false;
  }

  const domains = readFileSync(FILE_PARTIAL_DOMAINS).toString().split('\n');

  try {
    const parts = email.split('@');

    for (const domain of domains) {
      if (parts.length === 2 && parts[1].trim().endsWith(domain)) {
        return true;
      }
    }
  } catch(e) {
    logger.error(e);
  }

  return false;
}
