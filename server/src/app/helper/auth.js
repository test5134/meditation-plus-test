import _ from 'lodash-es';
import { USER_ROLES } from '../constants.js';

/**
 * Check if user has an admin role.
 *
 * @param {object} user The user to check
 */
export const isAdmin = (user) => [USER_ROLES.ADMIN, USER_ROLES.ADMIN_TEACHER]
  .includes(_.get(user, 'role', ''));

/**
 * Check if user has a teacher role.
 *
 * @param {object} user The user to check
 */
export const isTeacher = (user) => [USER_ROLES.TEACHER, USER_ROLES.ADMIN_TEACHER]
  .includes(_.get(user, 'role', ''));


/**
 * Middleware to restrict access of route to admin users only.
 */
export const authAdmin = (req, res, next) => {
  if (!req.isAuthenticated() || !isAdmin(req.user)) {
    res.sendStatus(401);
  } else {
    next();
  }
};

/**
 * Middleware to restrict access of route to teacher or admin users only.
 */
export const authTeacher = (req, res, next) => {
  if (!req.isAuthenticated() || !isTeacher(req.user)) {
    res.sendStatus(401);
  } else {
    next();
  }
};
