import Settings from '../models/settings.model.js';
import Appointment from '../models/appointment.model.js';
import moment from 'moment-timezone';
import { logger } from './logger.js';
import { round, get as gv } from 'lodash-es';
import { User } from '../models/user.model.js';
import { isAdmin } from './auth.js';
import { QUERY_TEACHER } from '../constants.js';

/**
 * Parse appointment hour stored as number
 * into hour and minute.
 */
const parseAppointmentHour = (rawHour) => ({
  hour: Math.floor(rawHour / 100),
  minute: rawHour % 100
});

/**
 * Calculates the minutes remaining to the appointment
 * of the current week. Expects that the teacher property is
 * populated and contains the 'timezone' value.
 *
 * @param {Appointment} appointment
 *
 * @return difference in minutes to the appointment.
 */
const minutesUntilAppointment = async (appointment, nowMoment = moment.utc()) => {
  if (!appointment || typeof appointment.hour !== 'number' || typeof appointment.weekDay !== 'number') {
    return Infinity;
  }

  const nowMomentTz = moment.tz(nowMoment, gv(appointment, 'teacher.timezone', ''));

  // create a Moment for appointment
  const { hour, minute } = parseAppointmentHour(appointment.hour);
  let appDateTimeSpecs = {
    weekday: appointment.weekDay,
    hour,
    minute,
    second: 0,
    millisecond: 0
  };

  // treat special cases which occur betwen SATURDAY and SUNDAY
  // because of the indexing from 0-6
  if (nowMomentTz.weekday() === 6 && appointment.weekDay === 0) {
    appDateTimeSpecs.weekday = 7; // now = SATURDAY, appointment = SUNDAY => take tomorror (0 + 7)
  } else if (nowMomentTz.weekday() === 0 && appointment.weekDay === 6) {
    appDateTimeSpecs.weekday = -1; // now = SUNDAY, appointment = SATURDAY => take yesterday (6 - 7)
  }

  const appMoment = nowMomentTz.clone().set(appDateTimeSpecs);
  return round(appMoment.diff(nowMomentTz, 'minutes', true), 2);
};

/**
 * Find an appointment that is scheduled for now for a given user.
 *
 * @param {User}    user               The user to make the lookup for
 *
 * @return A matching appointment object or null.
 */
const findCurrentAppointmentForUser = async (user) => {
  if (!user || !user._id) {
    return null;
  }

  let appointment = await Appointment
    .findOne({ user: user._id })
    .populate('teacher', 'timezone')
    .lean();

  if (!appointment) {
    return null;
  }

  // check if it's the correct time
  const settings = await Settings.findOne();
  const diffMins = await minutesUntilAppointment(appointment);
  return -1 * diffMins <= gv(settings, 'appointmentsToLateTolerance', 0)
    && diffMins <= gv(settings, 'appointmentsToEarlyTolerance', 0)
    ? appointment
    : null;
};

/**
 * Check whether the schedule page is set to
 * be disabled for the current moment.
 */
const isScheduleDisabled = async () => {
  try {
    const settings = await Settings.findOne();
    if (!settings || settings.disableAppointments !== true) {
      return false;
    }

    // if no start and end time are set, then the deactivation is treated as permanent
    if (!settings.disableAppointmentsFrom && !settings.disableAppointmentsUntil) {
      return true;
    }

    // check time range
    const tz = settings && !!moment.tz.zone(settings.disableAppointmentsTimezone)
      ? settings.disableAppointmentsTimezone
      : '';

    const momentFrom = moment.tz(settings.disableAppointmentsFrom, 'YYYY-MM-DD', true, tz);
    const momentUntil = moment.tz(settings.disableAppointmentsUntil, 'YYYY-MM-DD', true, tz);

    // in case a date is invalid, the deactivation is treated as invalid, as well
    if (!momentFrom.isValid() || !momentUntil.isValid()) {
      return false;
    }

    const momentNow = moment.tz(settings.disableAppointmentsTimezone);

    return momentNow.isSameOrAfter(momentFrom, 'day') && momentNow.isSameOrBefore(momentUntil, 'day');
  } catch (error) {
    logger.error('Failed to run isScheduleDisabled()', error);
    return false;
  }
};

/**
 * Format an hour number to a typical hour string.
 * I.e. 1800 -> "18:00", 700 -> "07:00"
 * @param {number} hour Hour as number
 */
const formatHour = (hour) => {
  const hourStr = '0000' + hour.toString();
  return hourStr.substr(-4, 2) + ':' + hourStr.substr(-2, 2);
};

/**
 * Middleware to check the request parameter 'teacherId'.
 *   - return 403 Unauthorized if not the same as the requesting user or not admin
 *   - return 404 If no user was found under the ID
 *   - populate req.teacher with corresponding user object if success
 */
const checkTeacherParam = async (req, res, next) => {
  try {
    if (!isAdmin(req.user) && req.params.teacherId !== req.user._id.toString()) {
      return res.sendStatus(403);
    }

    const teacher = await User
      .findOne({
        ...QUERY_TEACHER,
        _id: new String(req.params.teacherId)
      })
      .select('_id username name timezone country')
      .lean();

    if (!teacher) {
      return res.sendStatus(404);
    }

    req.teacher = teacher;

    next();
  } catch (err) {
    logger.error(req.url, err);
    res.status(500).json(err);
  }
};

export {
  parseAppointmentHour,
  minutesUntilAppointment,
  findCurrentAppointmentForUser,
  isScheduleDisabled,
  checkTeacherParam,
  formatHour
};
