import nodemailer from 'nodemailer';
import fs from 'fs';
import { logger } from './logger.js';
import { User } from '../models/user.model.js';

let transporter = nodemailer.createTransport({
  host: process.env.EMAIL_SMTP_HOST,
  port: process.env.EMAIL_SMTP_PORT,
  secure: true,
  auth: {
    user: process.env.EMAIL_ADDRESS,
    pass: process.env.EMAIL_PASSWORD
  }
});

transporter.verify((err) => {
  if (err) {
    logger.error('Failed to connect to SMTP server', err);
  } else {
    logger.info('Successfully connected to SMTP server');
  }
});

/**
 * Creates a message from a template. For each template there must exist
 * a folder named after the template containing a 'mail.html' and a 'mail.txt'
 * file in '/src/app/helper/mail-templates'.
 *
 * @param  {string} template      Name of the template / folder containing template files
 * @param  {Object} replacements  Object containing custom data to insert into message (has to be marked like '{{myProp}}' in the template)
 * @return {Object}               Object containing message as plain text and HTML file
 */
const createMessage = (template, replacements) => {
  // read html skeleton used for every email
  let mockupHTML = fs.readFileSync(process.cwd() + '/src/app/helper/mail-templates/mockup.html', 'utf8');

  // read customized content for email
  let plain = fs.readFileSync(process.cwd() + '/src/app/helper/mail-templates/' + template + '/mail.txt', 'utf8');
  let html = fs.readFileSync(process.cwd() + '/src/app/helper/mail-templates/' + template + '/mail.html', 'utf8');

  for (let key in replacements) {
    plain = plain.replace('{{' + key + '}}', replacements[key]);
    html = html.replace('{{' + key + '}}', replacements[key]);
  }

  return {
    plain: plain,
    html: mockupHTML.replace('{{content}}', html)
  };
};

/**
 * Populates a message with shared and required configurations
 * for sending the email correctly.
 */
const generateMessagePayload = (data = {}) => ({
  from: process.env.EMAIL_ADDRESS,
  ...data
});

export default {
  /**
   * Sends a newly registered user a link via email to verify the account
   *
   * @param  {string} userName    User's name
   * @param  {string} mailAddress User's mail address
   * @param  {string} token       User's activation token
   */
  sendActivationEmail: (user, callback = null) => {
    if (!transporter) {
      return;
    }

    const activationLink = process.env.PUBLIC_URL + '/login;verify=' + user.verifyToken;
    const message = createMessage('activate_account', {
      userName: user.name,
      activationLink: activationLink
    });

    transporter.sendMail(generateMessagePayload({
      to: user.local.email,
      subject: 'Please verify your email for Meditation+',
      text: message.plain
      // only use plain message for now to make sure that our html markup
      // is not the reason why emails are classified as spam
      // html: message.html
    }), callback);
  },
  sendRecoveryEmail: (user, callback = null) => {
    if (!transporter) {
      return;
    }

    const recoveryLink =
      process.env.PUBLIC_URL + '/reset-password;user=' +  user._id + ';token=' + user.verifyToken;

    const message = createMessage('recover_password', {
      userName: user.name,
      recoveryLink: recoveryLink
    });

    transporter.sendMail(generateMessagePayload({
      to: user.local.email,
      subject: 'Meditation+ Password Recovery',
      text: message.plain
      // html: message.html
    }), callback);
  },
  sendTestimonialNotification: (testimonial, callback = null) => {
    if (!transporter) {
      return;
    }

    User
      .find({
        role: 'ROLE_ADMIN',
        'notifications.testimonial': true
      })
      .then(res => {
        res.map(user => {
          let message = createMessage('testimonial_notification', {
            testimonialText: testimonial.text,
            reviewLink: process.env.PUBLIC_URL + '/admin/testimonials'
          });

          transporter.sendMail(generateMessagePayload({
            to: user.local.email,
            subject: 'Meditation+ New Testimonial',
            text: message.plain
            // html: message.html
          }), callback);
        });
      });
  },
  sendMail: (to, subject, text, callback) => transporter.sendMail(generateMessagePayload({
    to,
    subject,
    text
  }), callback)
};
