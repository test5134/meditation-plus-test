import Commitment from '../models/commitment.model.js';
import CommitmentUser from '../models/commitment-user.model.js';
import { ProfileHelper } from '../helper/profile.js';
import timezone from '../helper/timezone.js';
import mongoose from 'mongoose';
let ObjectId = mongoose.Types.ObjectId;
import { logger } from '../helper/logger.js';
import { COMMITMENT_TYPES } from '../constants.js';
import { authAdmin } from '../helper/auth.js';
import _ from 'lodash-es';

export default (app, router) => {

  /**
  * @api {get} /api/commitment Get all commitments
  * @apiName ListCommitments
  * @apiGroup Commitment
  *
  * @apiSuccess {Object[]} commitments             List of available commitments
  * @apiSuccess {String}   commitments.type        "daily", "weekly", ("monthly")
  * @apiSuccess {Number}   commitments.minutes     Minutes of meditation per type
  * @apiSuccess {User[]}   commitments.users       Committing users
  */
  router.get('/api/commitment', async (req, res) => {
    try {
      const result = await Commitment
        .find()
        .populate({
          path: 'users',
          model: 'CommitmentUser',
          populate: {
            path: 'user',
            model: 'User',
            select: 'name username lastMeditation timezone avatarImageToken'
          }
        })
        .lean()
        .then();

      for (const commitment of result) {
        if (!commitment['users']) {
          continue;
        }

        const noMeditationDaysTolerance = commitment.type === 'daily' ? 3 : 7;

        commitment.users = commitment.users.map(user => {
          const lastMeditation = _.get(user, 'user.lastMeditation');

          user.isActive = lastMeditation && timezone(user.user)
            .startOf('day')
            .subtract(noMeditationDaysTolerance, 'days')
            .isBefore(lastMeditation);

          return user;
        });
      }

      res.json(result);
    } catch (err) {
      logger.error(req.url, err);
      res.send(err);
    }
  });

  /**
  * @api {get} /api/commitment/:id Get all commitments of current user
  * @apiName GetCommitment
  * @apiGroup Commitment
  *
  * @apiSuccess {String}   type        "daily", "weekly", ("monthly")
  * @apiSuccess {Number}   minutes     Minutes of meditation per type
  */
  router.get('/api/commitment/user', async (req, res) => {
    try {
      const commitments = await CommitmentUser
        .find({
          user: new ObjectId(req.user._id)
        })
        .populate('commitment')
        .lean()
        .then();

      const result = {};

      if (commitments) {
        const pHelper = new ProfileHelper(req.user);

        for (let { commitment } of commitments) {
          // Calculate progress for all commitments
          commitment.reached = await pHelper.getCommitmentStatus(commitment);
          result[commitment._id] = commitment;
        }
      }

      res.json(result);
    } catch (err) {
      logger.error(req.url, err);
      res.send(err);
    }
  });

  /**
  * @api {get} /api/commitment/:id Get single commitment
  * @apiName GetCommitment
  * @apiGroup Commitment
  *
  * @apiSuccess {String}   type        "daily", "weekly", ("monthly")
  * @apiSuccess {Number}   minutes     Minutes of meditation per type
  */
  router.get('/api/commitment/:id', authAdmin, async (req, res) => {
    try {
      const result = await Commitment
        .findOne({ _id: new String(req.params.id) })
        .lean()
        .then();

      if (!result) return res.sendStatus(404);
      res.json(result);
    } catch (err) {
      logger.error(req.url, err);
      res.send(err);
    }
  });

  /**
  * @api {put} /api/commitment/:id Update commitment
  * @apiName UpdateCommitment
  * @apiGroup Commitment
  *
  * @apiParam {String}   type        "daily", "weekly", ("monthly")
  * @apiParam {Number}   minutes     Minutes of meditation per type
  */
  router.put('/api/commitment/:id', authAdmin, async (req, res) => {
    try {
      const commitment = await Commitment.findById(new String(req.params.id));

      if (!commitment) {
        return res.status(404).send('Commitment not found.');
      }

      const patch = {};

      if (req.body.type && COMMITMENT_TYPES.includes(req.body.type)) {
        patch['type'] = new String(req.body.type);
      }

      if (req.body.minutes && typeof req.body.minutes === 'number' && req.body.minutes > 0) {
        patch['minutes'] = new String(req.body.minutes);
      }

      if (Object.keys(patch).length === 0) {
        return res.status(400).send('No valid update values provided.');
      }

      await commitment.updateOne(patch);

      res.sendStatus(204);
    } catch (err) {
      logger.error(req.url, err);
      res.status(400).send(err);
    }
  });

  /**
  * @api {post} /api/commitment Add new commitment
  * @apiName AddCommitment
  * @apiGroup Commitment
  */
  router.post('/api/commitment', authAdmin, async (req, res) => {
    try {
      await Commitment.create({
        type: new String(req.body.type),
        minutes: new String(req.body.minutes)
      });

      res.sendStatus(201);
    } catch (err) {
      logger.error(req.url, err);
      res
        .status(err.name === 'ValidationError' ? 400 : 500)
        .send(err);
    }
  });


  /**
  * @api {post} /api/commitment/:id/commit Commit Commitment
  * @apiName CommitCommitment
  * @apiGroup Commitment
  *
  * @apiParam {String} id ObjectID of the Commitment
  */
  router.post('/api/commitment/:id/commit', async (req, res) => {
    try {
      const commitment = await Commitment.findById(new String(req.params.id));

      if (!commitment) {
        return res.status(404).json();
      }

      const existingCommit = await CommitmentUser.findOne({
        user: new String(req.user._id),
        commitment: new String(req.params.id)
      });

      if (existingCommit) {
        return res.status(400).send('Already commited.');
      }

      const newCommit = await CommitmentUser.create({
        user: req.user._id,
        commitment: commitment._id
      });

      await Commitment.updateOne({ _id: commitment._id }, {
        $push: { users: newCommit }
      });

      res.sendStatus(204);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
  * @api {post} /api/commitment/:id/uncommit Uncommit Commitment
  * @apiName UncommitCommitment
  * @apiGroup Commitment
  *
  * @apiParam {String} id ObjectID of the Commitment
  */
  router.post('/api/commitment/:id/uncommit', async (req, res) => {
    try {
      const commitment = await Commitment.findById(new String(req.params.id));

      if (!commitment) {
        return res.status(404).json();
      }

      const existingCommit = await CommitmentUser.findOne({
        user: new String(req.user._id),
        commitment: new String(req.params.id)
      });

      if (!existingCommit) {
        return res.status(400).send('Not commited.');
      }

      await Commitment.updateOne({
        _id: commitment._id
      }, {
        $pull: { users: { 'user._id': req.user._id } }
      });

      await CommitmentUser.deleteOne({ _id: existingCommit._id });

      // user not found
      res.status(204).json();
    } catch (err) {
      logger.error(req.url, err);
      res.status(400).send(err);
    }
  });

  /**
  * @api {delete} /api/commitment/:id Deletes commitment
  * @apiName DeleteCommitment
  * @apiGroup Commitment
  */
  router.delete('/api/commitment/:id', authAdmin, async (req, res) => {
    try {
      const result = await Commitment
        .find({ _id: new String(req.params.id) })
        .deleteOne()
        .exec();

      res.json(result);
    } catch (err) {
      logger.error(req.url, err);
      res.send(err);
    }
  });
};
