import chai from 'chai';
const { expect } = chai;
import moment from 'moment-timezone';
import { AuthedSupertest } from '../helper/authed-supertest.js';
import { isScheduleDisabled } from '../helper/appointment.js';
import Settings from '../models/settings.model.js';

let user = new AuthedSupertest();
let admin = new AuthedSupertest(
  'Admin User',
  'admin',
  'admin@sirimangalo.org',
  'password',
  'ROLE_ADMIN'
);


describe('Settings Routes', () => {
  let settings;

  user.authorize();
  admin.authorize();

  describe('GET /api/settings', () => {
    beforeEach(done => {
      Settings.deleteMany(() => {
        settings = new Settings();

        settings.save(err => {
          if (err) return done(err);
          done();
        });
      });
    });

    afterEach(() => {
      return Settings.deleteMany().exec();
    });

    it('should respond with settings entity', done => {
      user
        .get('/api/settings')
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.have.property('disableAppointmentsTimezone');
          expect(res.body.disableAppointmentsTimezone).to.equal('America/Toronto');
          done(err);
        });
    });
  });

  describe('POST /api/settings', () => {
    beforeEach(done => {
      Settings.deleteMany(() => {
        settings = new Settings();

        settings.save(err => {
          if (err) return done(err);
          done();
        });
      });
    });

    afterEach(() => {
      return Settings.deleteMany().exec();
    });

    it('should respond with 401 when not authorized', done => {
      user
        .put('/api/settings/disableAppointmentsTimezone')
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with 400 if no value is provided', done => {
      admin
        .put('/api/settings/disableAppointmentsTimezone')
        .expect(400)
        .end(err => done(err));
    });

    it('should respond with 204 if request is valid', done => {
      admin
        .put('/api/settings/disableAppointmentsTimezone')
        .send({
          value: 'America/Toronto'
        })
        .expect(204)
        .end(err => done(err));
    });
  });

  describe('POST /api/settings/disable-schedule', () => {
    before(done => {
      Settings.deleteMany(() => {
        settings = new Settings();

        settings.save(err => {
          if (err) return done(err);
          done();
        });
      });
    });

    after(() => {
      return Settings.deleteMany().exec();
    });

    it('should respond with 401 when not authorized', done => {
      user
        .post('/api/settings/disable-schedule')
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with 400 if invalid value for "disabled"', done => {
      admin
        .post('/api/settings/disable-schedule')
        .send({
          disabled: 'asdasdasd'
        })
        .expect(400)
        .end(err => done(err));
    });

    it('should respond with 400 if date is in invalid format (1)', done => {
      admin
        .post('/api/settings/disable-schedule')
        .send({
          disabled: true,
          dateFrom: '201933-01-03',
          dateTo: '2020-01-01'
        })
        .expect(400)
        .end(err => done(err));
    });

    it('should respond with 400 if date is in invalid format (2)', done => {
      admin
        .post('/api/settings/disable-schedule')
        .send({
          disabled: true,
          dateFrom: undefined,
          dateTo: '2020-01-01'
        })
        .expect(400)
        .end(err => done(err));
    });

    it('should respond with 400 if date is in invalid format (3)', done => {
      admin
        .post('/api/settings/disable-schedule')
        .send({
          disabled: true,
          dateFrom: '2020-01-01T00:00:00-05:00',
          dateTo: '2020-01-04T00:00:00-05:00'
        })
        .expect(400)
        .end(err => done(err));
    });

    it('should respond with 400 if invalid value for invalid date range (1)', done => {
      const now = moment.tz('America/Toronto');

      admin
        .post('/api/settings/disable-schedule')
        .send({
          disabled: true,
          dateFrom: now.clone().subtract(10, 'days'),
          dateTo: now.clone().subtract(3, 'day')
        })
        .expect(400)
        .end(err => done(err));
    });

    it('should respond with 400 if invalid value for invalid date range (2)', done => {
      const now = moment.tz('America/Toronto');

      admin
        .post('/api/settings/disable-schedule')
        .send({
          disabled: true,
          dateFrom: now.clone().add(10, 'days'),
          dateTo: now.clone().add(4, 'day')
        })
        .expect(400)
        .end(err => done(err));
    });

    it('should respond with 204 for enabling', done => {
      admin
        .post('/api/settings/disable-schedule')
        .send({
          disabled: false
        })
        .expect(204)
        .end(async err => {
          expect(await isScheduleDisabled()).to.be.false;
          done(err);
        });
    });

    it('should respond with 204 for disabling', done => {
      admin
        .post('/api/settings/disable-schedule')
        .send({
          disabled: true
        })
        .expect(204)
        .end(async err => {
          expect(await isScheduleDisabled()).to.be.true;
          done(err);
        });
    });

    it('should respond with 204 for disabling and setting date range (1)', done => {
      const now = moment.tz('America/Toronto');

      admin
        .post('/api/settings/disable-schedule')
        .send({
          disabled: true,
          dateFrom: now.clone().subtract(2, 'days').format('YYYY-MM-DD'),
          dateTo: now.clone().add(3, 'day').format('YYYY-MM-DD')
        })
        .expect(204)
        .end(async err => {
          expect(await isScheduleDisabled()).to.be.true;
          done(err);
        });
    });

    it('should respond with 204 for disabling and setting date range (2)', done => {
      const now = moment.tz('America/Toronto');

      admin
        .post('/api/settings/disable-schedule')
        .send({
          disabled: true,
          dateFrom: now.clone().add(2, 'days').format('YYYY-MM-DD'),
          dateTo: now.clone().add(10, 'days').format('YYYY-MM-DD')
        })
        .expect(204)
        .end(async err => {
          expect(await isScheduleDisabled()).to.be.false;
          done(err);
        });
    });

    it('should respond with 204 for disabling and setting date range (3)', done => {
      const now = moment.tz('America/Toronto');

      admin
        .post('/api/settings/disable-schedule')
        .send({
          disabled: true,
          dateFrom: now.clone().subtract(2, 'days').format('YYYY-MM-DD'),
          dateTo: now.clone().format('YYYY-MM-DD')
        })
        .expect(204)
        .end(async err => {
          expect(await isScheduleDisabled()).to.be.true;
          done(err);
        });
    });

    it('should respond with 204 for disabling and setting date range (4)', done => {
      const now = moment.tz('America/Toronto');

      admin
        .post('/api/settings/disable-schedule')
        .send({
          disabled: true,
          dateFrom: now.clone().add(2, 'months').format('YYYY-MM-DD'),
          dateTo: now.clone().add(3, 'months').format('YYYY-MM-DD')
        })
        .expect(204)
        .end(async err => {
          expect(await isScheduleDisabled()).to.be.false;
          done(err);
        });
    });

    it('should respond with 204 for enabling/toggling (1)', done => {
      admin
        .post('/api/settings/disable-schedule')
        .send({
          disabled: false
        })
        .expect(204)
        .end(async err => {
          expect(await isScheduleDisabled()).to.be.false;
          done(err);
        });
    });

    it('should respond with 204 for enabling/toggling (2)', done => {
      admin
        .post('/api/settings/disable-schedule')
        .send({
          disabled: true
        })
        .expect(204)
        .end(async err => {
          expect(await isScheduleDisabled()).to.be.true;
          done(err);
        });
    });
  });
});
