import { User } from '../models/user.model.js';
import safe from 'safe-regex';
import { logger } from '../helper/logger.js';
import { authAdmin } from '../helper/auth.js';
import { cleanBody } from '../helper/clean-body.js';
import { validateSchemaPartial } from '../helper/validate.js';

export default (app, router, io) => {

  /**
   * @api {get} /api/user Get user data
   * @apiName ListUser
   * @apiGroup User
   *
   * @apiSuccess {Object[]} users             List of user slots
   */
  router.get('/api/user', authAdmin, async (req, res) => {
    try {
      const result = await User
        .find()
        .exec();

      res.json(result);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {get} /api/user/online Get online user data
   * @apiName ListOnlineUser
   * @apiGroup User
   *
   * @apiSuccess {Object[]} users List of online user
   */
  router.get('/api/user/online', async (req, res) => {
    try {
      const userIds = [];

      [...io.sockets.sockets.values()]
        .filter(socket => !!socket.decoded_token)
        .map(socket => socket.decoded_token._id)
        .forEach(id => userIds.push(id));

      const result = await User
        .find({
          _id: { $in: userIds }
        }, 'name avatarImageToken _id lastMeditation country username')
        .lean()
        .exec();

      res.json(result);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {post} /api/user/seach Searches users
   * @apiName SearchUser
   * @apiGroup User
   *
   * @apiSuccess {Object[]} users Search result
   */
  router.post('/api/user/search', authAdmin, async (req, res) => {
    try {
      if (!req.body.term || !safe(req.body.term)) {
        return res.status(400).send('Invalid search term.');
      }

      const regex = new RegExp(`.*${new String(req.body.term)}.*`, 'i');
      const result = await User
        .find({
          $or: [
            {'username': regex},
            {'name': regex},
            {'local.email': regex}
          ]
        })
        .exec();

      res.json(result);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {get} /api/user/:id Get single user
   * @apiName GetUser
   * @apiGroup User
   */
  router.get('/api/user/:id', authAdmin, async (req, res) => {
    try {
      const result = await User
        .findOne({ _id: new String(req.params.id) })
        .lean();

      delete result.local.password;
      delete result.verifyToken;

      res.json(result);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {get} /api/user/username/:username Get single user by username
   * @apiName GetUserByUsername
   * @apiGroup User
   */
  router.get('/api/user/username/:username', authAdmin, async (req, res) => {
    try {
      const result = await User
        .findOne({ username: new String(req.params.username) })
        .lean();

      delete result.local.password;
      delete result.verifyToken;

      res.json(result);
    } catch (err) {
      logger.error(req.url, err);
      res.send(err);
    }
  });

  /**
   * @api {get} /api/user/mention/:search Get single username by search string
   * @apiName GetUsernameForMention
   * @apiGroup User
   */
  router.get('/api/user/mention/:search', async (req, res) => {
    try {
      const search = req.params.search ? req.params.search : '';

      if (!search || !safe(search)) {
        return res.status(400).send('Invalid search term.');
      }

      const result = await User
        .findOne({ username: new RegExp('^' + new String(search), 'i') })
        .then();

      res.json(result ? result.username : '');
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
  * @api {put} /api/user/:id Update user
  * @apiName UpdateUser
  * @apiGroup User
  */
  router.put('/api/user/:id', cleanBody, authAdmin, async (req, res) => {
    try {
      let user = await User.findById(new String(req.params.id));

      if (!user) {
        return res.status(404).send();
      }

      if (req.body.newPassword) {
        const password = user.generateHash(req.body.newPassword);
        req.body.local = { ...req.body.local, password };
        delete req.body.newPassword;
        delete req.body.newPasswordRepeat;
      }

      const patch = await validateSchemaPartial(req.body, User);

      await User.updateOne({ _id: user._id }, patch);

      res.sendStatus(204);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {delete} /api/user/:id Deletes user
   * @apiName DeleteUser
   * @apiGroup User
   */
  router.delete('/api/user/:id', authAdmin, async (req, res) => {
    try {
      const user = await User.findById(new String(req.params.id));
      await user.deleteOne();

      res.sendStatus(204);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });
};
