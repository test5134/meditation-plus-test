import { User } from '../models/user.model.js';
import Meditation from '../models/meditation.model.js';
import moment from 'moment-timezone';
import { logger } from '../helper/logger.js';
import { authAdmin } from '../helper/auth.js';
import { QUERY_ADMIN } from '../constants.js';

import mongoose from 'mongoose';
let ObjectId = mongoose.Types.ObjectId;

export default (app, router) => {

  /**
   * @api {get} /api/analytics-users Get statistics of all user accounts
   * @apiName GetGeneralStats
   * @apiGroup analytics
   *
   * @apiSuccess {Object}   userdata             Object containing all data
   * @apiSuccess {Number}   userdata.count       Count of all users
   * @apiSuccess {Object[]} userdata.admins      List of all admin accounts
   * @apiSuccess {Object[]} userdata.suspended   List of all suspended accounts
   * @apiSuccess {Number}   userdata.inactive    Count of inactive users
   */
  router.get('/api/analytics-users', authAdmin, async (req, res) => {
    try {
      const count = await User.estimatedDocumentCount();

      const admins = await User
        .find(QUERY_ADMIN, 'name avatarImageToken _id country username')
        .lean()
        .exec();

      const suspended = await User
        .find({
          suspendedUntil: { $gt: Date.now() }
        }, 'name avatarImageToken _id country username')
        .lean()
        .exec();

      const inactive = await User
        .find({
          // older than 3 months (90 days) 7776E6
          lastActive: { $lte: Date.now() - 7776E6 }
        })
        .countDocuments()
        .exec();

      res.json({
        count: count,
        admins: admins,
        suspended: suspended,
        inactive: inactive
      });
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {get} /api/analytics-countries Get statistics of the countries of all accounts
   * @apiName GetCountryStats
   * @apiGroup analytics
   *
   * @apiSuccess {Object[]} countries  List of all countries and their count of users
   */
  router.get('/api/analytics-countries', authAdmin, async (req, res) => {
    try {
      User
        .aggregate([
          {
            $group: {
              _id: '$country', count: { $sum: 1 }
            }
          },
          { $sort : { count : -1} }
        ], (err, countries) => {
          if (err) {
            res.status(500).send(err);
          } else {
            res.json(countries);
          }
        });
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {get} /api/analytics-timezones Get statistics of the top 10 timezones of all accounts
   * @apiName GetTimezoneStats
   * @apiGroup analytics
   *
   * @apiSuccess {Object[]} timezones  List of top 10 timezones and their count of users
   */
  router.get('/api/analytics-timezones', authAdmin, async (req, res) => {
    try {
      User
        .aggregate([
          {
            $group: {
              _id: '$timezone', count: { $sum: 1 }
            }
          },
          { $sort : { count : -1} },
          { $limit : 10 }
        ], (err, timezones) => {
          if (err) {
            res.status(500).send(err);
          } else {
            res.json(timezones);
          }
        });
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {get} /api/analytics-signups Get statistics about new accounts
   * @apiName GetSignupStats
   * @apiGroup analytics
   *
   * @apiSuccess {Object} signupChartData  Data for linechart
   */
  router.post('/api/analytics-signups', authAdmin, async (req, res) => {
    try {
      // Default interval: 1 day (= 864E5 ms)
      const interval = req.body.interval ? req.body.interval : 864E5;

      // Default interval: Today - 7 days (= 6048E5 ms)
      const minDate = req.body.minDate && req.body.minDate < Date.now()
        ? req.body.minDate
        : Date.now() - 6048E5;

      const dtFormat = req.body.format && typeof(req.body.format) === 'string'
        ? req.body.format
        : 'MM/DD/YY';

      let data = {
        data: [],
        labels: []
      };

      let dtEnd = Date.now();
      let dtStart = dtEnd - interval;

      while (dtStart >= minDate) {
        // Use MongoDB's id to find new users within the current timespan
        const minObjID = ObjectId(Math.floor(dtStart / 1000).toString(16) + '0000000000000000');
        const maxObjID = ObjectId(Math.floor(dtEnd / 1000).toString(16) + '0000000000000000');

        const userCount = await User
          .find({
            _id: { $gte: minObjID, $lte: maxObjID }
          })
          .countDocuments();

        data.data.push(userCount);
        data.labels.push(moment(dtEnd).format(dtFormat));

        dtStart -= interval;
        dtEnd -= interval;
      }

      data.data.reverse();
      data.labels.reverse();

      res.json(data);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {get} /api/analytics-signups Get statistics about number of meditation sessions
   * @apiName GetMeditationStats
   * @apiGroup analytics
   *
   * @apiSuccess {Object} meditationChartData  Data for linechart
   */
  router.post('/api/analytics-meditations', authAdmin, async (req, res) => {
    try {
      // Default interval: 1 day (= 864E5 ms)
      const interval = req.body.interval ? req.body.interval : 864E5;

      // Default interval: Today - 7 days (= 6048E5 ms)
      const minDate = req.body.minDate ? req.body.minDate : Date.now() - 6048E5;
      const dtFormat = req.body.format ? req.body.format : 'MM/DD/YY';

      let data = {
        data: [],
        labels: []
      };

      let dtEnd = Date.now();
      let dtStart = dtEnd - interval;

      while (dtStart >= minDate) {
        const meditationCount = await Meditation
          .find({
            end: { $gte: dtStart, $lte: dtEnd }
          })
          .countDocuments();

        data.data.push(meditationCount);
        data.labels.push(moment(dtEnd).format(dtFormat));

        dtStart -= interval;
        dtEnd -= interval;
      }

      data.data.reverse();
      data.labels.reverse();

      res.json(data);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });
};


