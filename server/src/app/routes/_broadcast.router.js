import Broadcast from '../models/broadcast.model.js';
import { logger } from '../helper/logger.js';
import { authAdmin } from '../helper/auth.js';
import { cleanBody } from '../helper/clean-body.js';

export default (app, router) => {

  /**
   * @api {get} /api/broadcast Get broadcast data
   * @apiName ListBroadcast
   * @apiGroup broadcast
   *
   * @apiSuccess {Object[]} broadcasts             List of broadcast slots
   * @apiSuccess {Number}   broadcasts.ended       End time
   * @apiSuccess {Number}   broadcasts.started     Start time
   * @apiSuccess {Object}   broadcasts.videoUrl    Youtube url
   */
  router.get('/api/broadcast', async (req, res) => {
    try {
      const result = await Broadcast
        .find()
        .sort({
          createdAt: 'desc'
        })
        .limit(30)
        .exec();

      res.json(result);
    } catch (err) {
      logger.error(req.url, err);
      res.status(400).send(err);
    }
  });

  /**
   * @api {get} /api/broadcast/:id Get single broadcast
   * @apiName GetBroadcast
   * @apiGroup Broadcast
   *
   * @apiSuccess {Date}     started
   * @apiSuccess {Date}     end
   * @apiSuccess {String}   url
   */
  router.get('/api/broadcast/:id', authAdmin, async (req, res) => {
    try {
      const result = await Broadcast
        .findOne({ _id: new String(req.params.id) })
        .lean()
        .then();

      res.json(result);
    } catch (err) {
      logger.error(req.url, err);
      res.send(err);
    }
  });

  /**
   * @api {put} /api/broadcast/:id Update broadcast
   * @apiName UpdateBroadcast
   * @apiGroup Broadcast
   */
  router.put('/api/broadcast/:id', cleanBody, authAdmin, async (req, res) => {
    try {
      let broadcast = await Broadcast.findById(new String(req.params.id));
      for (const key of Object.keys(req.body)) {
        if (!['started', 'ended', 'videoUrl'].includes(key)) {
          continue;
        }

        broadcast[key] = req.body[key];
      }
      await broadcast.save();

      res.sendStatus(204);
    } catch (err) {
      logger.error(req.url, err);
      res.status(400).send(err);
    }
  });

  /**
   * @api {delete} /api/broadcast/:id Deletes broadcast
   * @apiName DeleteBroadcast
   * @apiGroup Broadcast
   */
  router.delete('/api/broadcast/:id', authAdmin, async (req, res) => {
    try {
      const result = await Broadcast
        .find({ _id: new String(req.params.id) })
        .deleteOne()
        .exec();

      res.json(result);
    } catch (err) {
      logger.error(req.url, err);
      res.send(err);
    }
  });
};
