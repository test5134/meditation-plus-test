import Feedback from '../models/feedback.model.js';
import { logger } from '../helper/logger.js';
import { authAdmin } from '../helper/auth.js';
import request from 'superagent';

import mongoose from 'mongoose';
let ObjectId = mongoose.Types.ObjectId;

export default (app, router) => {

  /**
   * @api {post} /api/feedback Create feedback
   * @apiName CreateFeedback
   * @apiGroup feedback
   *
   * @apiParam {String}   text              Feedback text
   * @apiParam {Number}   [rating]          Optional Rating of app (1 to 5)
   * @apiParam {String}   [device]          Optional Primary device used ("desktop", "mobile", "tablet")
   * @apiParam {Boolean}  [anonymous=true]  Optional flag to allow association of feedback with submitting user
   */
  router.post('/api/feedback', async (req, res) => {
    try {
      if (!req.body.text) {
        return res.status(400).json({ error: 'Text missing' });
      }

      let feedback = { text: new String(req.body.text) };

      if (req.body.rating) {
        feedback = { ...feedback, rating: new String(req.body.rating) };
      }

      if (req.body.device) {
        feedback = { ...feedback, device: new String(req.body.device) };
      }

      if (req.body.anonymous === false) {
        feedback = { ...feedback, user: ObjectId(req.user._id) };
      }

      await Feedback.create(feedback);

      if (process.env.NODE_ENV !== 'test' && process.env.DISCORD_FEEDBACK_WEBHOOK) {
        const content = `_${!feedback.user ? '(anonymous)' : req.user.username}_ has given a `
          + `${feedback.rating ? `**${feedback.rating}/5** rating` : 'feedback'} and says:\n\n`
          + feedback.text.split('\n').map(line => `> ${line}`).join('\n')
          + (feedback.device ? `\n\nHer/His primary device is **${feedback.device}**.` : '\n');

        try {
          await request
            .post(process.env.DISCORD_FEEDBACK_WEBHOOK)
            .set('Content-Type', 'application/json')
            .send({
              username: 'Meditation+ Feedback Bot',
              content
            });
        } catch (err) {
          logger.error(err);
        }
      }

      res.status(201).send();
    } catch (err) {
      logger.error(req.url, err);
      res.status(err.name === 'ValidationError' ? 400 : 500).send(err);
    }
  });

  /**
   * @api {get} /api/feedback Get feedback
   * @apiName GetFeedback
   * @apiGroup feedback
   *
   * @apiParam {Number} [page=1]      Optional page number
   * @apiParam {Number} [perPage=10]  Optional size of returned items (1 to 100)
   */
  router.get('/api/feedback', authAdmin, async (req, res) => {
    try {
      let page = parseInt(req.query.page, 10);
      page = !isNaN(page) ? page : 1;

      let perPage = parseInt(req.query.perPage, 10);
      perPage = !isNaN(perPage) ? Math.min(Math.max(1, perPage), 100) : 10;

      const feedbackList = await Feedback
        .find()
        .populate('user', '_id username name avatarImageToken country')
        .sort({ createdAt: -1 })
        .skip((page - 1) * perPage)
        .limit(perPage);

      res.json(feedbackList);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {delete} /api/feedback/:feedbackId Delete a feedback
   * @apiName DeleteFeedback
   * @apiGroup feedback
   *
   * @apiParam {String} feedbackId The id of the feedback
   */
  router.delete('/api/feedback/:feedbackId', authAdmin, async (req, res) => {
    try {
      const deletedDoc = await Feedback.findByIdAndDelete(new String(req.params.feedbackId));
      res.status(deletedDoc ? 200 : 404).send();
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });
};
