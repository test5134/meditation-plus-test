import mongoose from 'mongoose';

let settingsSchema = mongoose.Schema({
  disableAppointmentsTimezone: {
    type: String,
    default: 'America/Toronto'
  },
  appointmentsRegistrationForerun: Number,
  appointmentsToEarlyTolerance: {
    type: Number,
    default: 5
  },
  appointmentsToLateTolerance: {
    type: Number,
    default: 15
  },
  disableAppointments: Boolean,
  disableAppointmentsFrom: String,
  disableAppointmentsUntil: String,
  livestreamInfo: String,
  questionInfo: String,
  notification: String
});

export default mongoose.model('Settings', settingsSchema);
