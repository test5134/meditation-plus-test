import mongoose from 'mongoose';
import { COMMITMENT_TYPES } from '../constants.js';

let commitmentSchema = mongoose.Schema({
  type: {
    type: String,
    required: true,
    enum: COMMITMENT_TYPES
  },
  minutes: { type: Number, required: true },
  users: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'CommitmentUser'
  }]
}, {
  timestamps: true
});

export default mongoose.model('Commitment', commitmentSchema);
