import mongoose from 'mongoose';

let commitmentUserSchema = mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  commitment: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Commitment',
    required: true
  }
}, {
  timestamps: true
});

export default mongoose.model('CommitmentUser', commitmentUserSchema);
