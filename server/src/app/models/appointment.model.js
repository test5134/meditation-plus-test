import mongoose from 'mongoose';

let appointmentSchema = mongoose.Schema({
  hour: Number,
  weekDay: Number,
  teacher: {
    type : mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  user: {
    type : mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
}, {
  timestamps: true
});

export default mongoose.model('Appointment', appointmentSchema);
