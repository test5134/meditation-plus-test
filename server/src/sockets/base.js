import Message from '../app/models/message.model.js';
import { logger } from '../app/helper/logger.js';
import jwt from 'jsonwebtoken';

// This file contains the most basic functionality for server Socket.io
// functionality.

export default (io) => {
  io.use((socket, next) => {
    if (socket.handshake.headers && socket.handshake.headers.cookie) {
      jwt.verify(socket.handshake.headers.cookie.replace('token=', ''), process.env.SESSION_SECRET, function(err, decoded) {
        if (err) return next(new Error('Authentication error'));
        socket.decoded_token = decoded;
        next();
      });
    } else {
      next(new Error('Authentication error'));
    }
  }).on('connection', async socket => {
    const userIds = new Set();

    [...io.sockets.sockets.values()]
      .filter(socket => !!socket.decoded_token)
      .map(socket => socket.decoded_token._id)
      .forEach(id => userIds.add(id));

    // send out update for onlinecounter
    socket.broadcast.emit('onlinecounter:changed', userIds.size);

    logger.info('a user connected:', socket.decoded_token._id);
    logger.info(socket.id + ' connected via ('+ socket.client.conn.transport.constructor.name +')');

    // fetch latest message to detect missed timespans on the client
    const latestMessage = await Message
      .find()
      .sort('-createdAt')
      .limit(1)
      .lean();

    socket.emit('connection', {
      latestMessage: latestMessage.length > 0 ? latestMessage[0] : null
    });

    // event for getting count of online users
    socket.on('onlinecounter:get', () => socket.emit('onlinecounter:get', userIds.size));

    socket.on('disconnect', () => {
      const userIds = new Set();

      [...io.sockets.sockets.values()]
        .filter(socket => !!socket.decoded_token)
        .map(socket => socket.decoded_token._id)
        .forEach(id => userIds.add(id));

      // send out update for onlinecounter
      socket.broadcast.emit('onlinecounter:changed', userIds.size);

      logger.info('a user disconnected');
    });
  });
};
