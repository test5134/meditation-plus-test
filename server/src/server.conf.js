import express from 'express';
import { Server } from 'socket.io';
import http from 'http';
import cors from 'cors';
import expressJwt from 'express-jwt';
import mongoose from 'mongoose';
import firebase from 'firebase-admin';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import methodOverride from 'method-override';
import passport from 'passport';
import base from './sockets/base.js';
import mongooseConf from './config/mongoose.conf.js';
import passportConf from './config/passport.conf.js';
import firebaseConf from './config/firebase.conf.js';
import routes from './app/routes.js';
import { createDirectories, PUBLIC_DIR } from './config/folders.conf.js';
import dotenv from 'dotenv';
dotenv.config({ path: process.cwd() + '/../.env' });

let app = express();
let server = http.createServer(app);
let io = new Server(server, {
  cors: {
    origin: process.env.PUBLIC_URL,
    credentials: true
  },
  transports: ['websocket']
});
let port = process.env.PORT || 3000;

base(io);
firebaseConf(firebase);
mongooseConf(mongoose);
passportConf(passport);
createDirectories();

if (process.env.NODE_ENV !== 'test') {
  // Log requests to console
  app.use(morgan('combined'));
}

app.use(cors({credentials: true, origin: process.env.PUBLIC_URL}));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(express.static(process.cwd() + '/../client'));
app.use(passport.initialize());

let router = express.Router();
routes(app, router, passport, io);

// We are going to protect /api routes with JWT and /auth/delete
app.use(['/api', '/auth/delete', '/auth/logout'], expressJwt({
  secret: process.env.SESSION_SECRET,
  algorithms: ['HS256'],
  getToken: req => req.cookies.token
}));

// Error handling. Needed to prevent the server from crashing
// (https://github.com/auth0/express-jwt/issues/194)
// eslint-disable-next-line no-unused-vars
app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401).send('invalid token');
  }
});

// Serve static files (used for avatar images)
app.use('/public', express.static(PUBLIC_DIR));

app.use('/', router);
server.listen(port);

export { app };
