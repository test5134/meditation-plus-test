## Communication

The development takes place in this GitLab repository. The communication between project participants happens either in the comments of an issue or Merge Request (MR), or at Discord. If you are interested in contributing, it is therefore recommended that you join the Discord server at https://discord.gg/DYNamMF6Kg and introduce yourself in the channel named "join-volunteer-team".

## Workflow

### 1. Ticket Assignment

- Please make sure you're assigned to a corresponding issue before writing your code. \*
- Please don't work on issues that are marked as blocked, "on hold" or requiring specification or confirmation.
- Check out issues marked as ["good first issue"](https://gitlab.com/sirimangalo/meditation-plus/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=good%20first%20issue) if you are new.

\* Generally any new MRs are welcome, but if opened without warning it may be rejected because it's a duplicate or unwanted change. Take a look at the issues and [opened or merged](https://gitlab.com/sirimangalo/dpr/merge_requests) MRs first.

### 2. Development / Best Practices

0. Please discuss unclear implementation details **before** writing your code, multiple iterations in the code review will take up much more time afterwards.
1. If you're not a member of this project, fork the repository.
1. Create your branch based on the latest master branch. Name your branch in the format `[type]/[issue number]-[branch name slug]`, i.e.

    ```shell
    git checkout -b feat/4711-user-registration
    ```

1. Follow our [code style guidelines](#code-style).
1. Write your code.
1. Run the tests and linting locally.
1. Commit your changes using a descriptive commit message that follows our
  [commit message guidelines](#commit-message-format):

     ```shell
     git commit -am 'feat(user): add registration possibility'
     ```

1. Push your branch:

    ```shell
    git push origin feat/4711-user-registration
    ```

### 3. Merge Request & Code Review

1. Create a MR to `master`.
    - Please make sure there are no merge conflicts and the pipelines pass.
    - Use the "Draft:" prefix if your code is not yet ready for review.
    - Name the MR like a [commit message](#commit) and mention the issue number you're working on in the description. If your MR closes the issue, use "Closes #4711".
1. Assign the MR to your code reviewer or write a notice that it needs attention.
1. Please respond to comments and apply or discuss change requests by the code reviewer(s).
    - re-assign the code reviewer once your done.

After approval, a maintainer is going to merge it with the master branch.

## Style Guidelines

### Code Style

We follow [Google's JavaScript Style Guide](https://google.github.io/styleguide/jsguide.html) with the addition of rules for TypeScript enforced by [Google TypeScript Style](https://github.com/google/gts).

### Commit Message Format

We follow the idea of semantic commit messages [[1](https://seesparkbox.com/foundry/semantic_commit_messages)][[2](https://karma-runner.github.io/0.10/dev/git-commit-msg.html)]. This leads to **more
readable messages**.

Each commit message consists of a **type**, a **scope** and a **subject**:

```
<type>(<scope>): <subject>
```

The **type** and **subject** are mandatory and the **scope** of the message is optional.

Any line of the commit message should not be longer than 100 characters! This allows the message to be easier to read on git tools.

**Examples:**

```
docs(users): add api documentation
fix: add missing property
chore: update dev dependencies to latest version
feat(login): add form validation for username and password
style: add missing semicolon
```

#### Type
Please stick to the following **types** in your commit messages:

* **chore**: Mostly maintenance. Changes that affect the build system, ci configuration or external dependencies, etc.
* **docs**: Changes to documentation
* **feat**: A new feature
* **fix**: A bug fix
* **refactor**: Changes like __restructuring code__ or __renaming variables__ that do not change the functionality of the code.
* **style**: Changes to the style of the code like __formatting__, __adding missing semi colons__, etc.
* **test**: Adding missing tests or correcting existing tests

#### Scope

The scope should be the part of the application that's affected (optional).

#### Subject

The subject contains a succinct description of the change:

* be in imperative, present tense (i.e. "change" instead of "changed" or "changes")
* not have the first letter capitalized
* not end with a `.` (dot)
