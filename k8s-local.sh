# Helper for local Kubernetes initialization

helm upgrade \
  --install \
  --set web.name="meditation-plus-dev" \
  --set web.clientImage="registry.gitlab.com/sirimangalo/meditation-plus/client:latest" \
  --set web.serverImage="registry.gitlab.com/sirimangalo/meditation-plus/server:latest" \
  --set web.environment="Dev" \
  --wait \
  meditation-dev \
  ./k8s/chart
