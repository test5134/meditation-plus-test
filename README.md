<div align="center">
![Logo](https://gitlab.com/sirimangalo/meditation-plus/-/raw/master/client/src/assets/icon/android-icon-144x144.png)

<h1>Meditation+</h1>
</div>


## Requirements

- [Node](https://nodejs.org)
- [Requirements of node-gyp](https://github.com/nodejs/node-gyp#on-unix)
- [MongoDB](https://www.mongodb.com/) 
Alternatively run this:
```sh
    docker run -d -p 27017:27017 --name med-plus \
    -e MONGO_INITDB_ROOT_USERNAME=mongo \
    -e MONGO_INITDB_ROOT_PASSWORD=mongo \
    mongo
```

## Quickstart

**Setup Client**

```
cd client/
yarn install
yarn start
```

You should be able to see the webapp at http://localhost:4200/ now. However, you still need to start the server as described below.

**Setup Server**

```
cd server/
yarn install
```

Next, add a server configuration:

```
cp ../.env.dist ../.env
```

Modify the content of `../.env` to match your environment. Usually only `MONGO_HOST` has to be configured.

You now can start the server:

```
yarn start
```

It will by default use the local port 3000.

**Populating Database with some data**

Creating new users locally will require you to manually set their `verified` field to `true` in the database. You can use following command to quickly populate the database with initial data:

```
yarn populate-dev
```

Now you should be able to login as `admin` or `tommy` (password: "password"). What data is used can be modified in `server/src/scripts/dev-data`.


**Troubleshooting**

If you see an error that looks like this, then please try downgrading node (using a version manager like n or nvm) to 14:

```
gyp ERR! build error
gyp ERR! stack Error: `make` failed with exit code: 2
gyp ERR! stack     at ChildProcess.onExit (/home/tommy/Dokumente/Code/meditation-plus/client/node_modules/node-gyp/lib/build.js:262:23)
gyp ERR! stack     at ChildProcess.emit (events.js:314:20)
gyp ERR! stack     at Process.ChildProcess._handle.onexit (internal/child_process.js:276:12)
gyp ERR! System Linux 5.8.14-200.fc32.x86_64
```

## Additional Information

### Testing Development Tips

To isolate a spec file per run, change the function name `it` to `fit` or `describe` to `fdescribe`.

### Additional configuration

For some features to work properly you need to further modify the `.env`:

#### Uploading avatar images

Please create a folder and set `STORAGE_DIR` to its absolute path.

#### Push notifications

For push notifications (i.e. for chat messages) to work locally, you need to first create a new [firebase](https://firebase.google.com/) project and then

- set `FIREBASE_DB` to the url which looks like `https://myproject.firebaseio.com`
- download the private server config and move it to `src/config/firebase.json`

**Note:** There might arise issues if the local app does not have `https://`

### API Documentation

You can view the latest API Documention in the [Build artifacts](https://gitlab.com/sirimangalo/meditation-plus/-/jobs/artifacts/master/file/server/doc/index.html?job=build%20docs).

### Updating Angular Material

To save the dependency of `node-sass`, our custom theme is built locally. Install `node-sass` (or sass) on your computer and run the following command to update the Angular Material Theme:
```
node-sass src/material-design.scss src/material-design.css
```

## Kubernetes

Meditation+ is deployed via [Kubernetes](https://kubernetes.io/) as a [Helm Chart](https://helm.sh/). To test modifications locally, you can install [Minikube](https://minikube.sigs.k8s.io/) and run `sh k8s-local.sh`. This should install Meditation+ in your local Kubernetes cluster.

You can now modify the files in `k8s/chart` to change deployment specifics.
