export const environment = {
  production: false,
  publicVapidKey: 'dev',
  fcm: null,
  apiUrl: window.location.origin
};
