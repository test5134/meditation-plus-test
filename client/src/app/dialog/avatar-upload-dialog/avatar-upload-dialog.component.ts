import { Component, Inject, ElementRef, ViewChild } from '@angular/core';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from '../../user/user.service';
import Cropper from 'cropperjs';

@Component({
  selector: 'app-avatar-upload-dialog',
  templateUrl: './avatar-upload-dialog.component.html',
  styleUrls: ['./avatar-upload-dialog.component.styl']
})
export class AvatarUploadDialogComponent {
  @ViewChild('cropBeforeUpload', {read: ElementRef}) cropBeforeUpload: ElementRef;
  @ViewChild('uploadFile', {read: ElementRef}) uploadFile: ElementRef;

  cropper: Cropper;
  loading = false;
  errorMsg = '';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AvatarUploadDialogComponent>,
    private snackbar: MatSnackBar,
    private userService: UserService
  ) {
  }

  readURL(event: any): void {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      if (!file.type.startsWith('image/')) {
        this.uploadFile.nativeElement.value = '';
        return;
      }

      const reader = new FileReader();
      reader.onload = () => {
        this.cropBeforeUpload.nativeElement.src = reader.result;

        const options = {
          viewMode: 3,
          initialAspectRatio: 1,
          aspectRatio: 1,
          rotatable: false,
          minCropBoxWidth: 320,
          minCropBoxHeight: 320,
        };

        this.cropper = new Cropper(this.cropBeforeUpload.nativeElement, options);

        const evtHandler = (evt) => {
          const imageData = this.cropper.getImageData();

          // fix: minCropBoxWidth applies to the canvas size, not the actual image
          // size. As a workaraound create a new cropper right after we now the
          // scaled value for the min dimensions that match the original image sizes.
          //
          // see:
          //   - https://github.com/fengyuanchen/cropper/issues/797
          //   - https://github.com/fengyuanchen/cropperjs/issues/210
          if (evt.target === window || options.minCropBoxWidth === 320) {
            options.minCropBoxWidth = 320 * (imageData.width / imageData.naturalWidth);
            options.minCropBoxHeight = 320 * (imageData.height / imageData.naturalHeight);
            this.cropper.destroy();
            this.cropper = new Cropper(this.cropBeforeUpload.nativeElement, options);
          }
        };

        this.cropBeforeUpload.nativeElement.addEventListener('ready', evt => evtHandler(evt));
        window.addEventListener('resize', evt => evtHandler(evt));
      };

      reader.onerror = () => {
        console.error(reader.error);
        this.errorMsg = reader.error.message;
      };

      reader.readAsDataURL(file);
    }
  }

  reset() {
    this.errorMsg = '';

    if (this.uploadFile) {
      this.uploadFile.nativeElement.value = '';
    }

    if (this.cropBeforeUpload) {
      this.cropBeforeUpload.nativeElement.src = '';
    }

    if (this.cropper) {
      this.cropper.destroy();
    }
  }

  submit() {
    if (!this.uploadFile || !this.uploadFile.nativeElement.files || !this.uploadFile.nativeElement.files.length) {
      return;
    }

    this.loading = true;
    this.errorMsg = '';

    // upload avatar
    const cropData = this.cropper.getData();
    const imageData = this.cropper.getImageData();

    // limit crop area by image boundaries
    const cropX = Math.min(Math.max(0, cropData.x), imageData.naturalWidth - cropData.width);
    const cropY = Math.min(Math.max(0, cropData.y), imageData.naturalHeight - cropData.width);

    this.userService
      .updateAvatar(this.uploadFile.nativeElement.files[0], {
        // library on server requires integer values...
        width: Math.floor(cropData.width),
        height: Math.floor(cropData.width),
        left: Math.floor(cropX),
        top: Math.floor(cropY)
      }, this.data.userId)
      .subscribe(
        res => {
          // pass new token for instant reload on client
          this.snackbar.open('Your profile image has been updated.');
          this.dialogRef.close(res.avatarSecret);
        },
        err => {
          this.loading = false;
          this.errorMsg = err.error && err.error.error
            ? err.error.error
            : 'An unknown error occured';
        }
      );
  }

}
