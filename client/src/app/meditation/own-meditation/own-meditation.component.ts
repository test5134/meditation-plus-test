import { Component, ChangeDetectionStrategy, ChangeDetectorRef, OnInit, OnDestroy } from '@angular/core';
import { Observable, interval, Subscription } from 'rxjs';
import { Meditation } from 'app/meditation/meditation';
import * as meditation from '../reducers/meditation.reducers';
import { AppState } from 'app/reducers';
import { Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import { CancelMeditation } from 'app/meditation/actions/meditation.actions';
import { DialogService } from 'app/dialog/dialog.service';

@Component({
  selector: 'app-own-meditation',
  templateUrl: './own-meditation.component.html',
  styleUrls: ['./own-meditation.component.styl'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OwnMeditationComponent implements OnInit, OnDestroy {

  ownSession$: Observable<Meditation | null>;
  activeMeditationsWithoutOwn$: Observable<Meditation[]>;
  subscriptions: Subscription;
  emptyFiller = Array;

  constructor(
    private store: Store<AppState>,
    private dialog: DialogService,
    public chDet: ChangeDetectorRef
  ) {
  }

  /**
   * Stopping active meditation session.
   */
  stop() {
    this.subscriptions.add(this.dialog.confirm(
      'Stop session?',
      'Are you sure you want to stop your session?'
    ).pipe(filter(val => !!val)).subscribe(() => this.store.dispatch(new CancelMeditation())));
  }

  /**
   * Rounds a number. Math.round isn't available in the template.
   * Needed for meditation progress.
   *
   * @param  {number} val Value to round
   * @return {number}     Rounded value
   */
  round(val: number): number {
    return Math.round(val);
  }
  ngOnInit() {
    this.activeMeditationsWithoutOwn$ = this.store.select(meditation.selectActiveWithoutOwn);
    this.ownSession$ = this.store.select(meditation.selectOwnSession);
    this.subscriptions = interval(500).subscribe(() => this.chDet.markForCheck());
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
