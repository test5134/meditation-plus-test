import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from './reducers';
import { selectTitle, selectSidenavOpen, selectConnectionProblems } from './reducers/global.reducers';
import { selectTokenExpired, selectId, selectAdmin, selectTeacher } from './auth/reducers/auth.reducers';
import { selectIsMeditating } from './meditation/reducers/meditation.reducers';
import { Logout } from './auth/actions/auth.actions';

@Component({
  selector: 'app-root',
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    './app.component.styl',
  ],
  templateUrl: './app.component.html'
})
export class AppComponent {
  @ViewChild('start') sidenav: any;

  name = 'Meditation+';
  title$: Observable<string>;
  connectionProblems$: Observable<boolean>;
  version$: Observable<string>;
  tokenExpired$: Observable<boolean>;
  userId$: Observable<string>;
  admin$: Observable<boolean>;
  teacher$: Observable<boolean>;
  isMeditating$: Observable<boolean>;

  constructor(
    private http: HttpClient,
    private titleService: Title,
    private store: Store<AppState>
  ) {
    this.title$ = this.store.select(selectTitle);
    this.tokenExpired$ = this.store.select(selectTokenExpired);
    this.userId$ = this.store.select(selectId);
    this.admin$ = this.store.select(selectAdmin);
    this.teacher$ = this.store.select(selectTeacher);
    this.connectionProblems$ = this.store.select(selectConnectionProblems);
    this.isMeditating$ = this.store.select(selectIsMeditating);

    this.title$.subscribe(title =>
      this.titleService.setTitle(title ? title : this.name)
    );

    this.store.select(selectSidenavOpen).pipe(
      filter(res => res && this.sidenav && this.sidenav._isClosed)
    ).subscribe(() => this.sidenav.open());

    this.version$ = this.http
      .get<any>('/assets/version.json?nocache')
      .pipe(map(res => res.version));
  }

  public logout() {
    this.store.dispatch(new Logout());
  }

  reload() {
    window.location.reload();
  }
}
