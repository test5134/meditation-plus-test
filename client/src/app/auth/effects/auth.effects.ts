import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { map, concatMap, catchError, tap } from 'rxjs/operators';
import { AuthService } from '../../shared/auth.service';
import * as auth from '../actions/auth.actions';
import * as jwtDecode from 'jwt-decode';
import { of } from 'rxjs';
import { Router } from '@angular/router';
import { UserService } from 'app/user';

@Injectable()
export class AuthEffect {
  constructor(
    private actions$: Actions,
    private service: AuthService,
    private router: Router,
    private userService: UserService
  ) {
  }

  @Effect()
  login$ = this.actions$
    .ofType<auth.Login>(auth.LOGIN)
    .pipe(
      map(action => action.payload),
      concatMap(payload =>
        this.service.login(payload.email, payload.password, payload.username, payload.acceptGdpr, payload.deleteAccount).pipe(
          map(res => [<any>jwtDecode(res.token), res.profile]),
          map(([res, profile]) => (new auth.LoginDone({
            tokenExpires: res.exp,
            profile
          }))),
          catchError(res => of(new auth.LoginError(res.error)))
        )
      ),
    );

  @Effect({ dispatch: false })
  loginDone$ = this.actions$
    .ofType<auth.LoginDone>(auth.LOGIN_DONE)
    .pipe(
      tap(() => this.router.navigate(['/home'])),
    );

  @Effect()
  reloadProfile$ = this.actions$
    .ofType<auth.ReloadProfile>(auth.RELOAD_PROFILE)
    .pipe(
      concatMap( () => this.userService.getProfile().pipe(
        map(profile => new auth.ReloadProfileDone(profile))
      ))
    );

  @Effect()
  signup$ = this.actions$
    .ofType<auth.Signup>(auth.SIGNUP)
    .pipe(
      map(action => action.payload),
      concatMap(payload =>
        this.service.signup(payload.name, payload.password, payload.email, payload.username, payload.acceptGdpr).pipe(
          map(res => (new auth.SignupDone(res))),
          catchError(res => of(new auth.SignupError(res.error || res.statusText || 'Unknown error')))
        )
      ),
    );

  @Effect()
  logout$ = this.actions$
    .ofType<auth.Logout>(auth.LOGOUT)
    .pipe(
      concatMap(() =>
        this.service.logout().pipe(
          map(() => (new auth.LogoutDone())),
          catchError(res => of(new auth.LogoutError(res)))
        )
      ),
    );

  @Effect({ dispatch: false })
  logoutDone$ = this.actions$
    .ofType<auth.LogoutDone>(auth.LOGOUT_DONE)
    .pipe(
      tap(() => this.router.navigate(['/login']))
    );
}
