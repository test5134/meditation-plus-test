import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Question } from '../question';
import * as moment from 'moment';

/**
 * Generates timestamp for youtube link.
 */
@Pipe({ name: 'broadcastUrl' })
export class BroadcastUrlPipe implements PipeTransform {

  constructor(
    private sanitizer: DomSanitizer
  ) {
  }

  private queryStringToObject(queryString: string): any {
    return queryString
      .replace(/(^\?)/, '')
      .split('&')
      .map(function(n) { return n = n.split('='), this[n[0]] = n[1], this; }.bind({}))[0];
  }

  private getYoutubeUrlParams(url: string): { v: string, t: number } {
    const query = url.includes('?')
      ? this.queryStringToObject(url.split('?')[1])
      : { v: '', t: 0 };

    if (url.includes('youtu.be')) {
      const queryStart = url.indexOf('.be/') + 4;
      let queryEnd = url.indexOf('?t=');
      queryEnd = queryEnd >= 0 ? queryEnd : url.length;
      query.v = url.substr(queryStart, queryEnd - queryStart);
    }

    return query;
  }

  /**
   * Get the relative answering time of a question in it's broadcast.
   */
  private getBroadcastDiff(value: Question): number {
    if (value.broadcast) {
      const broadcastStarted = moment(value.broadcast.started);
      const answering = value.answeringAt
        ? moment(value.answeringAt)
        : moment(value.answeredAt).subtract(30, 'seconds');
      const duration = moment.duration(answering.diff(broadcastStarted));

      return Math.max(0, Math.round(duration.asSeconds()));
    }

    return 0;
  }

  transform(value: Question): SafeResourceUrl {
    if (!value.videoUrl && (!value.broadcast || !value.broadcast.videoUrl)) {
      return null;
    }

    const urlParams = this.getYoutubeUrlParams(value.videoUrl ? value.videoUrl : value.broadcast.videoUrl);
    const videoId = urlParams.v;
    const relTime = value.videoUrl ? urlParams.t : this.getBroadcastDiff(value);

    return this.sanitizer.bypassSecurityTrustResourceUrl(
      `https://www.youtube.com/embed/${videoId}?start=${relTime}&autoplay=1&rel=0`
    );
  }

}
