import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { concatMap, map, withLatestFrom, switchMap, tap, catchError } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { QuestionService } from '../question.service';
import * as _ from 'lodash';
import {
  LoadAnsweredQuestions,
  LOAD_ANSWERED,
  LoadAnsweredQuestionsDone,
  LoadUnansweredQuestions,
  LOAD_UNANSWERED,
  LoadUnansweredQuestionsDone,
  PostQuestion,
  POST,
  PostQuestionDone,
  LikeQuestion,
  LikeQuestionDone,
  AnsweringQuestion,
  LIKE,
  ANSWERING,
  AnsweringQuestionDone,
  CancelAnsweringQuestion,
  CancelAnsweringQuestionDone,
  CANCEL_ANSWERING,
  ANSWER,
  AnswerQuestion,
  AnswerQuestionDone,
  DeleteQuestion,
  DELETE,
  DeleteQuestionDone,
  loadAnsweredQuestionInitialQuery
} from '../actions/question.actions';
import { selectAnsweredPage, selectLastFilter } from '../reducers/question.reducer';
import { MatSnackBar } from '@angular/material';
import { of } from 'rxjs';
import { ThrowError } from 'app/actions/global.actions';

@Injectable()
export class QuestionEffect {
  constructor(
    private actions$: Actions,
    private service: QuestionService,
    private store: Store<AppState>,
    private snackbar: MatSnackBar
  ) {
  }

  @Effect()
  loadAnswered$ = this.actions$
    .ofType<LoadAnsweredQuestions>(LOAD_ANSWERED)
    .pipe(
      map(action => action.payload),
      withLatestFrom(this.store.select(selectAnsweredPage)),
      concatMap(([payload, page]) =>
        this.service.getQuestions(true, page, payload.query).pipe(
          map(questions => new LoadAnsweredQuestionsDone({
            ...payload,
            questions
          })),
          catchError(error => of(new ThrowError({ error })))
        )
      )
    );

  @Effect()
  loadUnanswered$ = this.actions$
    .ofType<LoadUnansweredQuestions>(LOAD_UNANSWERED)
    .pipe(
      concatMap(() =>
        this.service.getQuestions().pipe(
          map(questions => new LoadUnansweredQuestionsDone(questions)),
          catchError(error => of(new ThrowError({ error })))
        )
      )
    );

  @Effect()
  post$ = this.actions$
    .ofType<PostQuestion>(POST)
    .pipe(
      concatMap(action =>
        this.service.post(action.payload).pipe(
          tap(() => this.snackbar.open('Your question has been posted.')),
          map(() => new PostQuestionDone()),
          catchError(error => of(new ThrowError({ error })))
        )
      )
    );

  @Effect()
  like$ = this.actions$
    .ofType<LikeQuestion>(LIKE)
    .pipe(
      concatMap(action =>
        this.service.like(action.payload).pipe(
          map(() => new LikeQuestionDone()),
          catchError(error => of(new ThrowError({ error })))
        )
      )
    );

  @Effect()
  answering$ = this.actions$
    .ofType<AnsweringQuestion>(ANSWERING)
    .pipe(
      concatMap(action =>
        this.service.answering(action.payload).pipe(
          map(() => new AnsweringQuestionDone()),
          catchError(error => of(new ThrowError({ error })))
        )
      )
    );

  @Effect()
  cancelAnswering$ = this.actions$
    .ofType<CancelAnsweringQuestion>(CANCEL_ANSWERING)
    .pipe(
      concatMap(action =>
        this.service.unanswering(action.payload).pipe(
          map(() => new CancelAnsweringQuestionDone()),
          catchError(error => of(new ThrowError({ error })))
        )
      )
    );

  @Effect()
  answer$ = this.actions$
    .ofType<AnswerQuestion>(ANSWER)
    .pipe(
      concatMap(action =>
        this.service.answer(action.payload).pipe(
          tap(() => this.snackbar.open('The question has been answered.')),
          map(() => new AnswerQuestionDone()),
          catchError(error => of(new ThrowError({ error })))
        )
      )
    );

  @Effect()
  delete$ = this.actions$
    .ofType<DeleteQuestion>(DELETE)
    .pipe(
      concatMap(action =>
        this.service.delete(action.payload).pipe(
          tap(() => this.snackbar.open('The question has been deleted.')),
          map(() => new DeleteQuestionDone()),
          catchError(error => of(new ThrowError({ error })))
        )
      )
    );

  @Effect()
  socket$ =  this.service.getSocket()
    .pipe(
      withLatestFrom(this.store.select(selectLastFilter)),
      switchMap(([_unused, lastFilter]) => {
        const actions: any[] = [new LoadUnansweredQuestions()];

        // reload answered questions when no custom search was done
        if (_.isEqual(lastFilter, loadAnsweredQuestionInitialQuery)) {
          actions.push(new LoadAnsweredQuestions({
            intention: 'search',
            query: lastFilter
          }));
        }

        return actions;
      })
    );
}
