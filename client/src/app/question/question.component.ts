import {
  Component,
  EventEmitter,
  Output,
  ChangeDetectionStrategy
} from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { debounceTime, concatMap, map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import { selectAdmin } from '../auth/reducers/auth.reducers';
import { Observable, timer } from 'rxjs';
import { Question } from './question';
import { SettingsService } from '../shared';
import {
  LoadAnsweredQuestionsQuery,
  LoadAnsweredQuestions,
  PostQuestion,
  LoadUnansweredQuestions,
  loadAnsweredQuestionInitialQuery
} from './actions/question.actions';
import {
  selectUnanswered,
  selectAnswered,
  selectNoMorePages,
  selectLoadingAnswered,
  selectLoadingUnanswered,
  selectLoadedInitially,
  selectPosting
} from './reducers/question.reducer';

@Component({
  selector: 'question',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './question.component.html',
  styleUrls: [
    './question.component.styl'
  ]
})
export class QuestionComponent {

  @Output() loadingFinished: EventEmitter<any> = new EventEmitter<any>();

  unanswered$: Observable<Question[]>;
  answered$: Observable<Question[]>;
  noMorePages$: Observable<boolean>;
  loadingAnswered$: Observable<'search' | 'more' | false>;
  loadingUnanswered$: Observable<boolean>;
  loadedInitially$: Observable<boolean>;
  posting$: Observable<boolean>;
  questionInfo$: Observable<string>;

  showEmojiSelect = false;
  tabIndex = 0;
  loadedAnsweredTab = false;
  showForm = false;
  answeredSearch = '';
  showSearchOptions = false;

  // searching for suggestions
  currentSearch = '';
  formQuestion: FormGroup;
  formSearch: FormGroup;
  search: FormControl = new FormControl('');
  question: FormControl = new FormControl('');

  // params for answered tab
  searchParams: LoadAnsweredQuestionsQuery = {...loadAnsweredQuestionInitialQuery};

  admin$: Observable<boolean>;

  constructor(
    private store: Store<AppState>,
    private settings: SettingsService,
    fb: FormBuilder
  ) {
    this.admin$ = store.select(selectAdmin);
    this.unanswered$ = store.select(selectUnanswered);
    this.answered$ = store.select(selectAnswered);
    this.noMorePages$ = store.select(selectNoMorePages);
    this.loadingAnswered$ = store.select(selectLoadingAnswered);
    this.loadingUnanswered$ = store.select(selectLoadingUnanswered);
    this.loadedInitially$ = store.select(selectLoadedInitially);
    this.posting$ = store.select(selectPosting);

    this.formQuestion = fb.group({ 'question': this.question });

    this.question.valueChanges
      .pipe(debounceTime(400))
      .subscribe(val => this.currentSearch = val);

    this.store.dispatch(new LoadUnansweredQuestions());

    this.questionInfo$ = timer(0, 10000).pipe(
      concatMap(() => this.settings.get()),
      map(val => val.questionInfo)
    );
  }

  selectChange(target) {
    this.tabIndex = target.index;

    if (this.tabIndex === 1 && !this.loadedAnsweredTab) {
      this.loadAnsweredQuestions('search');
    }
  }

  emojiSelect(evt) {
    this.question.setValue(this.question.value + ':' + evt + ':');
    this.showEmojiSelect = false;
  }

  loadAnsweredQuestions(intention: 'search' | 'more') {
    this.loadedAnsweredTab = true;
    this.store.dispatch(new LoadAnsweredQuestions({
      intention,
      query: {
        ...this.searchParams,
        search: this.answeredSearch
      }
    }));
  }

  sendQuestion(evt) {
    evt.preventDefault();

    if (!this.question.value) {
      return;
    }

    this.store.dispatch(new PostQuestion(this.question.value));
    this.question.setValue('');
  }

  toggleSearchOptions() {
    this.showSearchOptions = !this.showSearchOptions;
  }
}
