import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppointmentService } from '../../appointment';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'appointment-form',
  templateUrl: './appointment-form.component.html'
})
export class AppointmentFormComponent implements OnInit {
  teacherId = '';

  time = '';
  day = 0;

  loading = false;
  errorMsg = '';

  constructor(
    private appointmentService: AppointmentService,
    private router: Router,
    private route: ActivatedRoute,
    private snackbar: MatSnackBar
  ) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.teacherId = params.get('teacher');
    });
  }

  submit() {
    if (!this.time) {
      return;
    }

    const appointment = {
      weekDay: this.day,
      hour: parseInt(this.time.replace(':', ''), 10)
    };

    this.loading = true;
    this.appointmentService
      .create(this.teacherId, appointment)
      .subscribe(
        () => {
          this.snackbar.open('The appointment has been saved successfully.');
          this.router.navigate(['/admin/appointments']);
        },
        err => {
          this.errorMsg = err.error;
          this.loading = false;
        },
        () => this.loading = false
      );
  }
}
