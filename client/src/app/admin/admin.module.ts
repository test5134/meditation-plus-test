import { NgModule } from '@angular/core';
import { SharedModule } from '../shared';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AdminIndexComponent } from './admin-index.component';
import { AppointmentAdminComponent } from './appointment/appointment-admin.component';
import { AppointmentFormComponent } from './appointment/appointment-form.component';
import { CommitmentAdminComponent } from './commitment/commitment-admin.component';
import { CommitmentFormComponent } from './commitment/commitment-form.component';
import { BroadcastAdminComponent } from './broadcast/broadcast-admin.component';
import { AnalyticsService } from './analytics/analytics.service';
import { BroadcastFormComponent } from './broadcast/broadcast-form.component';
import { BroadcastService } from './broadcast/broadcast.service';
import { TestimonialAdminComponent } from './testimonial/testimonial-admin.component';
import { UserAdminComponent } from './user/user-admin.component';
import { UserAdminFormComponent } from './user/user-admin-form.component';
import { UserModule } from '../user';
import { ProfileModule } from '../profile';
import { EmojiModule } from '../emoji';
import { MomentModule } from 'ngx-moment';
import { AnalyticsComponent } from './analytics/analytics.component';
import { WorldChartComponent } from './analytics/worldmap-chart/worldmap-chart.component';
import { adminRoutes } from './admin.routes';
import { ChartModule } from 'primeng/chart';
import { DialogModule } from '../dialog/dialog.module';
import { VideoSuggestionsComponent } from './video-suggestions/video-suggestions.component';
import { VideoSuggestionsService } from './video-suggestions/video-suggestions.service';
import { UserTextListModule } from '../user-text-list/user-text-list.module';
import { FeedbackAdminComponent } from './feedback/feedback-admin.component';
import { FeedbackCardComponent } from './feedback/feedback-card.component';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(adminRoutes),
    FormsModule,
    ReactiveFormsModule,
    UserModule,
    ProfileModule,
    EmojiModule,
    MomentModule,
    ChartModule,
    DialogModule,
    UserTextListModule
  ],
  providers: [
    BroadcastService,
    AnalyticsService,
    VideoSuggestionsService
  ],
  declarations: [
    AdminComponent,
    AdminIndexComponent,
    AnalyticsComponent,
    AppointmentAdminComponent,
    AppointmentFormComponent,
    CommitmentFormComponent,
    CommitmentAdminComponent,
    BroadcastFormComponent,
    BroadcastAdminComponent,
    TestimonialAdminComponent,
    UserAdminComponent,
    UserAdminFormComponent,
    WorldChartComponent,
    VideoSuggestionsComponent,
    FeedbackAdminComponent,
    FeedbackCardComponent
  ],
  exports: [
    AdminComponent,
    AdminIndexComponent,
    AppointmentAdminComponent,
    AppointmentFormComponent,
    CommitmentFormComponent,
    CommitmentAdminComponent,
    BroadcastFormComponent,
    BroadcastAdminComponent,
    TestimonialAdminComponent,
    UserAdminComponent,
    UserAdminFormComponent,
    WorldChartComponent
  ]
})
export class AdminModule { }
