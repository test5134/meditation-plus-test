import { Component, ViewEncapsulation } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'app/reducers';
import { SetTitle } from '../actions/global.actions';

@Component({
  selector: 'admin',
  encapsulation: ViewEncapsulation.None,
  template: `<router-outlet></router-outlet>`,
  styleUrls: [ './admin.component.styl' ]
})
export class AdminComponent {

  constructor(store: Store<AppState>) {
    store.dispatch(new SetTitle('Administration'));
  }
}
