import {
  async,
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick,
} from '@angular/core/testing';
import { Type } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ConfirmDialogComponent } from 'app/dialog/confirm-dialog/confirm-dialog.component';

import { FeedbackAdminComponent } from './feedback-admin.component';
import { FeedbackCardComponent } from './feedback-card.component';
import { FeedbackService } from 'app/feedback/feedback.service';
import {
  FakeFeedbackService,
  mockFeedback,
} from 'app/feedback/testing/fake-feedback.service';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { fakeRequestTime, fakeWaitTime } from 'app/shared/testing/fake-times';

describe('FeedbackAdminComponent', () => {
  let component: FeedbackAdminComponent;
  let fixture: ComponentFixture<FeedbackAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        FeedbackAdminComponent,
        FeedbackCardComponent,
        ConfirmDialogComponent,
      ],
      imports: [SharedModule, NoopAnimationsModule],
      providers: [
        {
          provide: FeedbackService,
          useClass: FakeFeedbackService,
        },
        {
          provide: Store,
          useValue: {
            dispatch: () => {},
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(FeedbackAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create and fetch first set of feedbacks', () => {
    expect(component).toBeTruthy();
    expect(component.feedbacks.length).toBe(10);
    expect(component.page).toBe(1);
    expect(component.feedbackDeleting).toBe(null);
    expect(component.loading).toBe(false);
    expect(component.fetchedAll).toBe(false);
  });

  it('should set loading flag to true when fetching feedbacks and back to false after response received', fakeAsync(() => {
    component.loadNext();
    expect(component.loading).toBe(true);
    tick(fakeWaitTime);
    expect(component.loading).toBe(false);
  }));

  it('should increment page count on successfull fetch of feedbacks', fakeAsync(() => {
    component.loadNext();
    tick(fakeWaitTime);
    expect(component.page).toBe(2);
  }));

  it('should concat fetched feedbacks on successful fetch', fakeAsync(() => {
    component.loadNext();
    tick(fakeWaitTime);
    expect(component.feedbacks.length).toBe(15);
  }));

  it('should set fetchedAll flag to true if the received feedback is less than the perPage setting', fakeAsync(() => {
    component.loadNext();
    tick(fakeWaitTime);
    expect(component.fetchedAll).toBe(true);
  }));

  it('should toggle the deleting flag when deleting feedback', fakeAsync(() => {
    component['deleteFeedback'](mockFeedback);
    expect(component.feedbackDeleting).toBe(mockFeedback);
    tick(fakeWaitTime);
    expect(component.feedbackDeleting).toBe(null);
  }));

  it('after delete should fetch current amount of feedbacks to refresh current list', fakeAsync(() => {
    component.loadNext();
    tick(fakeWaitTime);

    const feedbackSrvc = fixture.debugElement.injector.get<FeedbackService>(
      FeedbackService as Type<FeedbackService>
    );

    const mockFeedbacks = [
      mockFeedback,
      mockFeedback,
      mockFeedback,
      mockFeedback,
      mockFeedback,
      mockFeedback,
      mockFeedback,
      mockFeedback,
      mockFeedback,
      mockFeedback,
      mockFeedback,
      mockFeedback,
      mockFeedback,
      mockFeedback,
    ];

    const spy = spyOn(feedbackSrvc, 'getFeedback').and.returnValue(
      of(mockFeedbacks).pipe(delay(fakeRequestTime))
    );

    component['deleteFeedback'](mockFeedback);
    tick(fakeWaitTime);
    expect(spy).toHaveBeenCalledWith(1, 15);
    expect(component.feedbacks).toBe(mockFeedbacks);
  }));
});
