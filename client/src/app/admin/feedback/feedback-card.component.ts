import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Feedback } from 'app/feedback/feedback';

@Component({
  selector: 'feedback-card',
  templateUrl: './feedback-card.component.html',
  styleUrls: ['./feedback-card.component.styl'],
})
export class FeedbackCardComponent {
  @Input() deleting: boolean;
  @Input() feedback: Feedback;

  @Output() delete = new EventEmitter<any>();

  ratingArray = Array;

  constructor() {}
}
