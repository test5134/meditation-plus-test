import { Component, OnInit } from '@angular/core';
import { Feedback } from 'app/feedback/feedback';
import { FeedbackService } from 'app/feedback/feedback.service';
import { mergeMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from 'app/reducers/index';
import { ThrowError } from 'app/actions/global.actions';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ConfirmDialogComponent } from 'app/dialog/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'feedback-admin',
  templateUrl: './feedback-admin.component.html',
  styleUrls: ['./feedback-admin.component.styl'],
})
export class FeedbackAdminComponent implements OnInit {
  private readonly perPage = 10;
  private dialogRef: MatDialogRef<ConfirmDialogComponent> = null;

  page = 0;
  feedbacks: Feedback[] = [];
  feedbackDeleting: Feedback = null;
  loading = false;
  fetchedAll = false;

  constructor(
    private feedbackSrvc: FeedbackService,
    private store: Store<AppState>,
    private dialog: MatDialog
  ) {}

  loadNext(): void {
    this.loading = true;
    this.feedbackSrvc
      .getFeedback(this.page + 1, this.perPage)
      .subscribe((response) => {
        this.loading = false;
        this.feedbacks = this.feedbacks.concat(response);
        this.page++;

        this.fetchedAll = response.length < this.perPage;
      });
  }

  confirmDelete(feedback: Feedback) {
    this.dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        text: 'Are you sure you want to delete this feedback?',
      },
    });

    const subscription = this.dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.deleteFeedback(feedback);
      }

      subscription.unsubscribe();
      this.dialogRef = null;
    });
  }

  private deleteFeedback(feedback: Feedback): void {
    this.feedbackDeleting = feedback;
    this.feedbackSrvc
      .deleteFeedback(feedback._id)
      .pipe(
        mergeMap(() => this.feedbackSrvc.getFeedback(1, this.feedbacks.length))
      )
      .subscribe(
        (feedbacks) => {
          this.feedbackDeleting = null;
          this.feedbacks = feedbacks;
        },
        ({ error: message }) => {
          this.feedbackDeleting = null;
          this.store.dispatch(new ThrowError({ message }));
        }
      );
  }

  ngOnInit() {
    this.loadNext();
  }
}
