import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: [
    './reset-password.component.styl'
  ]
})
export class ResetPasswordComponent implements OnInit {

  message: string;
  error: string;
  loading: boolean;
  success: boolean;

  email: string;
  userId: string;
  token: string;

  password: string;
  password2: string;

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.userId = this.route.snapshot.params['user'];
    this.token = this.route.snapshot.params['token'];
  }

  sendMail(evt) {
    evt.preventDefault();

    if (!this.email) {
      return;
    }

    this.loading = true;

    this.authService.resetPasswordInit(this.email)
      .subscribe(
        () => {
          this.message = 'An email with instructions has been sent to your account.';
          this.error = '';
        },
        err => {
          this.message = '';
          this.error = err.error;
          this.loading = false;
        },
        () => this.loading = false
      );
  }

  resetPassword(evt) {
    evt.preventDefault();

    if (!this.userId || this.password !== this.password2) {
      return;
    }

    this.loading = true;

    this.authService.resetPassword(this.userId, this.token, this.password)
      .subscribe(
        () => this.success = true,
        err => {
          this.error = err.error;
          this.loading = false;
        },
        () => this.loading = false
      );
  }
}
