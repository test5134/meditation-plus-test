import { Directive, ElementRef, Input, OnChanges } from '@angular/core';
import { ApiConfig } from '../../api.config';
import { UserAvatarSize } from '../shared/user-avatar/user-avatar-size';

@Directive({
  selector: '[avatar]'
})
export class AvatarDirective implements OnChanges {
  @Input() thumbnailSize: UserAvatarSize = UserAvatarSize.Lg;
  @Input() username = '';
  @Input() token = '';

  constructor(private elementRef: ElementRef) {}

  ngOnChanges() {
    const sizeMap = {
      sm: 40,
      md: 80,
      lg: 160,
    };

    if (!this.username || !this.token) {
      this.elementRef.nativeElement.srcset =
        `/assets/img/profile-${sizeMap[this.thumbnailSize]}.jpg 1x,
         /assets/img/profile-${sizeMap[this.thumbnailSize] * 2}.jpg 2x,`;

      // Fallback for older browsers
      this.elementRef.nativeElement.src = `/assets/img/profile-${sizeMap[this.thumbnailSize]}.jpg`;

      return;
    }

    // Different sizes for HiDPI support
    this.elementRef.nativeElement.srcset =
    `${ApiConfig.url}/public/${this.username}-${this.token}-${sizeMap[this.thumbnailSize]}.jpeg 1x,
     ${ApiConfig.url}/public/${this.username}-${this.token}-${sizeMap[this.thumbnailSize] * 2}.jpeg 2x,`;

    // Fallback for older browsers
    this.elementRef.nativeElement.src =
      `${ApiConfig.url}/public/${this.username}-${this.token}-${sizeMap[this.thumbnailSize]}.jpeg`;
  }
}
