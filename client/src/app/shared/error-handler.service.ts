import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class ErrorHandlerService {

  public constructor(private snackbar: MatSnackBar) {
  }

  handle(message?: string, error?: any): void {
    this.snackbar.open(
      message || 'An error occurred while performing your action.',
      'RELOAD APP',
      {
        panelClass: ['error-snackbar'],
        duration: null
      }
    ).onAction().subscribe(() => window.location.reload());

    if (error) {
      console.error(error);
    }
  }
}
