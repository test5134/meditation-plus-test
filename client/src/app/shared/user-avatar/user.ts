export interface User {
    _id: string;
    username: string;
    name: string;
    country?: string;
    avatarImageToken?: string;
    hidden?: boolean;
    lastMeditation?: string;
}
