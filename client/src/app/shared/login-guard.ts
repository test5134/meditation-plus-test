import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import { selectIsLoggedIn, selectTokenExpired } from '../auth/reducers/auth.reducers';
import { withLatestFrom, map, tap, take } from 'rxjs/operators';

@Injectable()
export class LoginGuard implements CanActivate {
  constructor(private router: Router, private store: Store<AppState>) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.store.select(selectIsLoggedIn).pipe(
      take(1),
      withLatestFrom(this.store.select(selectTokenExpired)),
      map(([loggedIn, expired]) => !loggedIn || expired),
      tap(result => {
        if (!result) {
          this.router.navigate(['/home']);
        }
      })
    );
  }
}
