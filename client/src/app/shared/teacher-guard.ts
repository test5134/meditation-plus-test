import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanLoad,
  Route
} from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import { selectIsLoggedIn, selectTokenExpired, selectTeacher } from '../auth/reducers/auth.reducers';
import { withLatestFrom, map, tap, take } from 'rxjs/operators';

@Injectable()
export class TeacherGuard implements CanActivate, CanLoad {
  constructor(
    private router: Router,
    private store: Store<AppState>
  ) {}

  check() {
    return this.store.select(selectIsLoggedIn).pipe(
      take(1),
      withLatestFrom(this.store.select(selectTokenExpired)),
      withLatestFrom(this.store.select(selectTeacher)),
      map(([[loggedIn, expired], teacher]) => loggedIn && !expired && teacher),
      tap(result => {
        if (!result) {
          this.router.navigate(['login']);
        }
      })
    );
  }

  canLoad(route: Route): boolean | Observable<boolean> | Promise<boolean> {
    return this.check();
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.check();
  }
}
