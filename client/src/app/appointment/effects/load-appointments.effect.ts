import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { concatMap, map, catchError, withLatestFrom, filter } from 'rxjs/operators';
import { AppointmentService } from 'app/appointment/appointment.service';
import {
  LOAD_APPOINTMENTS,
  LoadAppointments,
  LoadAppointmentsDone,
  LoadAppointmentsError, LOAD_APPOINTMENTS_DONE, SelectTeacher, ReloadAppointmentsTable, RELOAD_APPOINTMENTS_TABLE, LOAD_APPOINTMENTS_ERROR
} from '../actions/appointment.actions';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'app/reducers';
import { selectAllTeachers, selectCurrentTeacher, selectOwnAppointment } from '../reducers/appointment.reducers';
import { AppointmentUtils } from '../appointment.utils';
import { find } from 'lodash';
import { ThrowError } from 'app/actions/global.actions';
import { selectId, selectTeacher } from 'app/auth/reducers/auth.reducers';

@Injectable()
export class LoadAppointmentsEffect {
  constructor(
    private actions$: Actions,
    private store$: Store<AppState>,
    private service: AppointmentService
  ) {
  }

  @Effect()
  loadAppointments$ = this.actions$
    .ofType<LoadAppointments>(LOAD_APPOINTMENTS)
    .pipe(
      concatMap(() =>
        this.service.getAll().pipe(
          map(res => new LoadAppointmentsDone(res)),
          catchError(error => of(new LoadAppointmentsError(error)))
        )
      )
    );

  @Effect()
  loadAppointmentsError$ = this.actions$
    .ofType<LoadAppointmentsError>(LOAD_APPOINTMENTS_ERROR)
    .pipe(
      map(action => action.payload),
      map((error) => new ThrowError(error))
    );

  @Effect()
  loadAppointmentsDone$ = this.actions$
    .ofType<LoadAppointmentsDone>(LOAD_APPOINTMENTS_DONE)
    .pipe(
      map(() => new ReloadAppointmentsTable())
    );

  @Effect()
  reloadAppointmentsTable$ = this.actions$
    .ofType<ReloadAppointmentsTable>(RELOAD_APPOINTMENTS_TABLE)
    .pipe(
      withLatestFrom(
        this.store$.select(selectId),
        this.store$.select(selectTeacher),
        this.store$.select(selectAllTeachers),
        this.store$.select(selectOwnAppointment),
        this.store$.select(selectCurrentTeacher)
      ),
      map(([action, userId, isTeacher, teachers, ownAppointment, currentTeacher]) => {
        // choose the table to display

        let profile = null;

        // table manually selected
        if (currentTeacher) {
          profile = find(teachers, ({ _id }) => _id === currentTeacher._id);
        }

        // table of own appointment
        if (!profile && ownAppointment) {
          profile = find(teachers, ({ _id }) => _id === ownAppointment['teacher']);
        }

        // own table if user is teacher
        if (!profile && isTeacher) {
          profile = find(teachers, ({ _id }) => _id === userId);
        }

        // default table
        if (!profile) {
          profile = find(
            teachers,
            ({ username }) => username === AppointmentUtils.DEFAULT_TEACHER_USERNAME
          );
        }

        return profile ? profile._id : '';
      }),
      filter(currentTeacher => !!currentTeacher),
      map(currentTeacher => new SelectTeacher(currentTeacher))
    );
}
