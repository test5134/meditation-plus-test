import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { concatMap, map, catchError, withLatestFrom, filter } from 'rxjs/operators';
import { AppointmentService } from 'app/appointment/appointment.service';
import {
  CheckOwnAppointment,
  CheckOwnAppointmentDone,
  CheckOwnAppointmentError,
  CHECK_OWN_APPOINTMENT
} from '../actions/appointment.actions';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'app/reducers';
import { selectOwnAppointment } from '../reducers/appointment.reducers';

@Injectable()
export class CheckOwnAppointmentEffect {
  constructor(
    private actions$: Actions,
    private service: AppointmentService,
    private store: Store<AppState>,
  ) {
  }

  @Effect()
  checkOwnAppointment$ = this.actions$
    .ofType<CheckOwnAppointment>(CHECK_OWN_APPOINTMENT)
    .pipe(
      withLatestFrom(
        this.store.select(selectOwnAppointment)
      ),
      filter(([any, ownAppointment]) => !!ownAppointment),
      concatMap(() =>
        this.service.getOwnAppointmentStatus().pipe(
          map(res => new CheckOwnAppointmentDone(res)),
          catchError(error => of(new CheckOwnAppointmentError(error)))
        )
      )
    );
}
