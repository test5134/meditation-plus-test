import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { map} from 'rxjs/operators';
import {
  SET_HOUR_FORMAT,
  SET_TIMEZONE,
  SetTimezone,
  SetHourFormat, ReloadAppointmentsTable
} from '../actions/appointment.actions';

@Injectable()
export class ChangeTimeEffect {
  constructor(
    private actions$: Actions
  ) {
  }

  @Effect()
  setTimezone$ = this.actions$
    .ofType<SetTimezone>(SET_TIMEZONE)
    .pipe(
      map(() => new ReloadAppointmentsTable())
    );

  @Effect()
  setHourFormat$ = this.actions$
    .ofType<SetHourFormat>(SET_HOUR_FORMAT)
    .pipe(
      map(() => new ReloadAppointmentsTable())
    );
}
