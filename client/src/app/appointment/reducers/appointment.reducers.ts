import * as appointment from '../actions/appointment.actions';
import * as _ from 'lodash';
import * as moment from 'moment-timezone';
import { AppState } from 'app/reducers';
import { createSelector } from '@ngrx/store';
import { Appointment, OwnAppointmentStatus } from '../appointment';
import { AppointmentUtils } from '../appointment.utils';

export interface AppointmentState {
  ownAppointment: Appointment;
  ownAppointmentStatus: OwnAppointmentStatus;
  loadingInitially: boolean;
  loadingError: boolean;
  allAppointments: Appointment[];
  allTeachers: any[];
  selectedTeacher: any;
  selectedAppointsLocalHours: string[];
  selectedAppointsByLocalDay: { [key: string]: Appointment[] };
  selectedAppointsByLocalHour: { [key: string]: Appointment[] };
  localTimezone: string;
  localHourFormat: string;
  warnings: string[];
}

export const initialAppointmentState: AppointmentState = {
  loadingInitially: true,
  loadingError: false,
  allAppointments: [],
  allTeachers: [],
  ownAppointment: null,
  ownAppointmentStatus: null,
  localTimezone: moment.tz.guess(),
  localHourFormat: 'HH:mm',
  selectedTeacher: null,
  selectedAppointsLocalHours: [],
  selectedAppointsByLocalDay: null,
  selectedAppointsByLocalHour: null,
  warnings: []
};

export function appointmentReducer(
  state = initialAppointmentState,
  action: appointment.Actions
): AppointmentState {
  switch (action.type) {
    case appointment.CHECK_OWN_APPOINTMENT_DONE: {
      return {
        ..._.cloneDeep(state),
        ownAppointmentStatus: action.payload
      };
    }

    case appointment.LOAD_APPOINTMENTS: {
      return {
        ..._.cloneDeep(state),
        loadingError: false,
        loadingInitially: !state.allAppointments
          || state.allAppointments.length === 0,
        warnings: []
      };
    }

    case appointment.LOAD_APPOINTMENTS_ERROR: {
      return {
        ..._.cloneDeep(state),
        loadingError: true,
        loadingInitially: false,
        warnings: []
      };
    }

    case appointment.LOAD_APPOINTMENTS_DONE: {
      const { teachers, ownAppointment, appointments } = action.payload;
      const { localTimezone, localHourFormat } = state;

      const warnings = AppointmentUtils.findWarningsForFutureAppointments(
        ownAppointment,
        teachers,
        localTimezone,
        localHourFormat
      );

      // make sure default teacher is first item
      let allTeachers = [];
      for (const teacher of teachers) {
        if (teacher['username'] === AppointmentUtils.DEFAULT_TEACHER_USERNAME) {
          allTeachers = [ teacher, ...allTeachers ];
        } else {
          allTeachers.push(teacher);
        }
      }

      return {
        ..._.cloneDeep(state),
        loadingInitially: false,
        loadingError: false,
        ownAppointment,
        allTeachers,
        allAppointments: appointments,
        warnings
      };
    }

    case appointment.SELECT_TEACHER: {
      const selectedTeacher = state.allTeachers.find(
        teacher => _.get(teacher, '_id', 'UNK') === action.payload
      );

      if (!selectedTeacher) {
        return state;
      }

      const appointments = state.allAppointments.filter(
        appoint => _.get(appoint, 'teacher', 'UNK') === action.payload
      );

      const {
        appointmentsByHour,
        appointmentsByDay,
        appointmentHours
      } = AppointmentUtils.groupAppointments(
        appointments,
        state.localTimezone,
        state.localHourFormat
      );

      return {
        ..._.cloneDeep(state),
        selectedTeacher,
        selectedAppointsLocalHours: appointmentHours,
        selectedAppointsByLocalDay: appointmentsByDay,
        selectedAppointsByLocalHour: appointmentsByHour
      };
    }

    case appointment.SET_TIMEZONE: {
      const { ownAppointment, allTeachers, localHourFormat } = state;
      const warnings = AppointmentUtils.findWarningsForFutureAppointments(
        ownAppointment,
        allTeachers,
        action.payload,
        localHourFormat
      );

      return {
        ..._.cloneDeep(state),
        localTimezone: action.payload,
        warnings
      };
    }

    case appointment.SET_HOUR_FORMAT: {
      const { ownAppointment, allTeachers, localTimezone } = state;
      const warnings = AppointmentUtils.findWarningsForFutureAppointments(
        ownAppointment,
        allTeachers,
        localTimezone,
        action.payload
      );

      return {
        ..._.cloneDeep(state),
        localHourFormat: action.payload,
        warnings
      };
    }

    default: {
      return state;
    }
  }
}

// Selectors for easy access
export const selectAppointment = (state: AppState) => state.appointment;
export const selectAllTeachers = createSelector(selectAppointment, (state: AppointmentState) => state.allTeachers);
export const selectCurrentTeacher = createSelector(selectAppointment, (state: AppointmentState) => state.selectedTeacher);
export const selectOwnAppointment = createSelector(selectAppointment, (state: AppointmentState) => state.ownAppointment);
export const selectOwnAppointmentStatus = createSelector(selectAppointment, (state: AppointmentState) => state.ownAppointmentStatus);
export const selectLocalTimezone = createSelector(selectAppointment, (state: AppointmentState) => state.localTimezone);
export const selectAppointsLocalHours = createSelector(selectAppointment, (state: AppointmentState) => state.selectedAppointsLocalHours);
export const selectAppointsByLocalDay = createSelector(selectAppointment, (state: AppointmentState) => state.selectedAppointsByLocalDay);
export const selectAppointsByLocalHour = createSelector(selectAppointment, (state: AppointmentState) => state.selectedAppointsByLocalHour);
export const selectLoadingAppointments = createSelector(selectAppointment, (state: AppointmentState) => state.loadingInitially);
export const selectLoadingError = createSelector(selectAppointment, (state: AppointmentState) => state.loadingError);
export const selectWarnings = createSelector(selectAppointment, (state: AppointmentState) => state.warnings);
