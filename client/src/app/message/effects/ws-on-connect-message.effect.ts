import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Store, Action } from '@ngrx/store';
import { AppState } from 'app/reducers';
import { withLatestFrom, concatMap, map } from 'rxjs/operators';
import * as moment from 'moment';
import { Message } from 'app/message/message';
import * as _ from 'lodash';
import { WebsocketOnConnect, WS_ON_CONNECT, WebsocketOnConnectPayload, SyncMessages } from 'app/message/actions/message.actions';
import { selectMessageList } from 'app/message/reducers/message.reducers';
import { WebsocketService } from 'app/shared';
import { Observable, of } from 'rxjs';

@Injectable()
export class WsOnConnectMessageEffect {
  constructor(
    private actions$: Actions,
    private store$: Store<AppState>,
    private wsService: WebsocketService
  ) {
  }

  @Effect()
  wsReceiveConnect = this.wsService.onConnected()
    .pipe(
      concatMap(data => of(new WebsocketOnConnect(data)))
    );

  @Effect()
  wsOnConnect$ = this.actions$
    .ofType<WebsocketOnConnect>(WS_ON_CONNECT)
    .pipe(
      map(action => action.payload),
      withLatestFrom(
        this.store$.select(selectMessageList)
      ),
      concatMap(([payload, messages]) =>
        mapOnConnectToSync(payload, messages)
      )
    );
}

export function mapOnConnectToSync(
  payload: WebsocketOnConnectPayload,
  messages: Message[]
): Observable<Action> {
  const last = _.last(messages);

  let actionMessages = { type: 'NO_ACTION' };

  if (last && !moment(last.createdAt).isSame(payload.latestMessage.createdAt)) {
    actionMessages = new SyncMessages({
      index: messages.length - 1,
      from: last.createdAt,
      to: moment(payload.latestMessage.createdAt)
    });
  }

  return of(actionMessages);
}
