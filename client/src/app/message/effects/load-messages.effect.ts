import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { MessageService } from 'app/message/message.service';
import { concatMap, map, tap } from 'rxjs/operators';
import { Message } from 'app/message/message';
import * as _ from 'lodash';
import { LOAD, LoadMessages, LoadMessagesDone } from 'app/message/actions/message.actions';
import { of } from 'rxjs';

@Injectable()
export class LoadMessageEffect {
  constructor(
    private actions$: Actions,
    private service: MessageService
  ) {
  }

  @Effect()
  load$ = this.actions$
    .ofType<LoadMessages>(LOAD)
    .pipe(
      map(action => action.payload),
      concatMap(page =>
        this.service.getRecent(page).pipe(
          tap(this.setLastMessage),
          concatMap(messages => of(new LoadMessagesDone({ messages, page })))
        )
      )
    );

  private setLastMessage(messages: Message[]) {
    if (messages.length === 0) {
      return;
    }
    const lastMessage = _.last(messages);
    window.localStorage.setItem(
      'lastMessage',
      lastMessage.createdAt.toString()
    );
    window.localStorage.setItem(
      'lastMessageId',
      lastMessage._id
    );
  }
}
