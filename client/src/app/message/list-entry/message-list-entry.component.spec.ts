import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MessageListEntryComponent } from './message-list-entry.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from '../../shared/material.module';
import { MomentModule } from 'ngx-moment';
import { EmojiModule } from '../../emoji';
import { LinkyModule } from 'angular-linky';
import { FakeMessageService } from '../testing/fake-message.service';
import { MessageService } from '../message.service';
import { By } from '@angular/platform-browser';
import { addMatchers } from '../../../testing/jasmine-matchers';
import { Component, OnInit, DebugElement } from '@angular/core';
import { UserTextListModule } from 'app/user-text-list/user-text-list.module';
import { SharedModule } from '../../shared';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Store } from '@ngrx/store';
import { MockStore } from 'testing/mock.store';

@Component({
  template: `
    <message-list-entry [message]="message"></message-list-entry>`
})
class TestHostComponent implements OnInit {
  message: any;

  public ngOnInit() {
    this.message = {
      text: 'hello :grin:',
      user: {
        username: 'test-user',
        name: 'test user',
        gravatarHash: 'user-gravatar'
      },
      country: 'us'
    };
  }
}


describe('MessageListEntryComponent', () => {
  let fixture: ComponentFixture<TestHostComponent>;
  let testHost: TestHostComponent;
  let el: DebugElement;

  beforeEach(async(() => {
    addMatchers();
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        RouterTestingModule,
        MomentModule,
        LinkyModule,
        EmojiModule,
        UserTextListModule,
        SharedModule,
        HttpClientTestingModule
      ],
      declarations: [
        MessageListEntryComponent,
        TestHostComponent
      ],
      providers: [
        {provide: MessageService, useClass: FakeMessageService},
        {provide: Store, useClass: MockStore}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    testHost = fixture.componentInstance;
    fixture.detectChanges();
    el = fixture.debugElement.query(By.css('user-text-list-entry'));
  });

  it('should be created', () => {
    expect(testHost).toBeTruthy();
  });
  it('should have emoji', () => {
    expect(el.nativeElement.querySelector('i').className).toBe('e1a-grin');
  });
  it('should have profile image', () => {
    const img = el.nativeElement.querySelector('user-avatar');
    expect(img).toBeTruthy();
  });
  it('should have text', () => {
    const text = el.query(By.css('span.text'));
    expect(text).toHaveText('hello');
  });
});

