import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import 'hammerjs';
import { MomentModule } from 'ngx-moment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
/*
 * Platform and Environment providers/directives/pipes
 */
import { ROUTES } from './app.routes';
// App is our top level component
import { AppComponent } from './app.component';
import { LoginComponent } from './login';
import { NotFoundComponent } from './not-found';
import { LiveComponent } from './live';
import { UserModule } from './user';
import { ProfileModule } from './profile';
import { OnlineComponent } from './online';
import { CommitmentComponent } from './commitment';
import { UpdateComponent } from './update';
import { ResetPasswordComponent } from './reset-password';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { appReducers, metaReducers } from './reducers';
import { environment } from 'environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { SharedModule } from './shared';
import { SettingsService } from './shared/settings.service';
import { UserService } from './user/user.service';
import { MessageService } from './message/message.service';
import { QuestionService } from './question/question.service';
import { MeditationService } from './meditation/meditation.service';
import { CommitmentService } from './commitment/commitment.service';
import { AppointmentService } from './appointment/appointment.service';
import { TestimonialService } from './testimonial/testimonial.service';
import { LiveService } from './live/live.service';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { HttpInterceptorService } from './shared/http-interceptor.service';
import { AuthEffect } from './auth/effects/auth.effects';
import { DialogModule, DIALOGS } from './dialog/dialog.module';
import { ErrorHttpInterceptorService } from './shared/error-http-interceptor.service';
import { GlobalEffect } from './effects/global.effects';
import { FeedbackModule } from './feedback/feedback.module';

// Application wide providers
const APP_PROVIDERS = [
  SettingsService,
  UserService,
  MessageService,
  QuestionService,
  MeditationService,
  CommitmentService,
  AppointmentService,
  TestimonialService,
  LiveService,
  { provide: LocationStrategy, useClass: PathLocationStrategy },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: HttpInterceptorService,
    multi: true
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorHttpInterceptorService,
    multi: true
  }
];

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    LoginComponent,
    NotFoundComponent,
    LiveComponent,
    OnlineComponent,
    CommitmentComponent,
    UpdateComponent,
    ResetPasswordComponent,
  ],
  imports: [
    BrowserModule,
    MomentModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SharedModule,
    RouterModule.forRoot(ROUTES),
    StoreModule.forRoot(appReducers, { metaReducers }),
    EffectsModule.forRoot([
      AuthEffect,
      GlobalEffect
    ]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    // Application Modules
    UserModule,
    ProfileModule,
    BrowserAnimationsModule,
    DialogModule,
    FeedbackModule
  ],
  entryComponents: DIALOGS,
  providers: [
    APP_PROVIDERS
  ]
})
export class AppModule {
}
