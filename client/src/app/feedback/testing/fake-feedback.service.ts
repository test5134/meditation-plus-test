import { Feedback } from '../feedback';
import { DeviceType } from '../device-type';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { fakeRequestTime } from 'app/shared/testing/fake-times';

export const mockFeedback = {
  createdAt: '2021-02-15T02:04:35.820Z',
  device: DeviceType.Tablet,
  rating: 3,
  text: 'mockFeedback',
  updatedAt: '2021-02-15T02:04:35.820Z',
  __v: 0,
  _id: '6029d6b3f1c2ca5880e1b98e',
};

export const mockFeedbacks = [
  mockFeedback,
  mockFeedback,
  mockFeedback,
  mockFeedback,
  mockFeedback,
  mockFeedback,
  mockFeedback,
  mockFeedback,
  mockFeedback,
  mockFeedback,
  mockFeedback,
  mockFeedback,
  mockFeedback,
  mockFeedback,
  mockFeedback,
];

export class FakeFeedbackService {
  getFeedback(page: number, perPage: number = 10): Observable<Feedback[]> {
    return of(mockFeedbacks.slice((page - 1) * perPage, page * perPage)).pipe(
      delay(fakeRequestTime)
    );
  }

  postFeedback(feedback: Feedback): Observable<Feedback> {
    return of(feedback);
  }

  deleteFeedback(id: number): Observable<any> {
    return of(null).pipe(delay(fakeRequestTime));
  }
}
