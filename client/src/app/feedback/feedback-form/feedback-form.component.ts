import { Component, OnInit } from '@angular/core';
import { FeedbackService } from '../feedback.service';
import { MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { ThrowError } from '../../actions/global.actions';

@Component({
  selector: 'app-feedback-form',
  templateUrl: './feedback-form.component.html',
  styleUrls: ['./feedback-form.component.styl'],
})
export class FeedbackFormComponent implements OnInit {
  feedbackForm: FormGroup;
  isSaving = false;
  submitted = false;
  stars = Array;

  constructor(
    private feedbackSrvc: FeedbackService,
    private formBuilder: FormBuilder,
    private store: Store<AppState>,
    public dialogRef: MatDialogRef<FeedbackFormComponent>
  ) {}

  submit() {
    if (this.feedbackForm.valid) {
      this.isSaving = true;
      this.feedbackSrvc.postFeedback(this.feedbackForm.value).subscribe(
        () => {
          this.isSaving = false;
          this.submitted = true;
        },
        (error) => this.store.dispatch(new ThrowError(error))
      );
    }
  }

  ngOnInit() {
    this.feedbackForm = this.formBuilder.group({
      text: ['', [Validators.required, Validators.maxLength(1000)]],
      anonymous: false,
      rating: null,
      device: null,
    });
  }
}
