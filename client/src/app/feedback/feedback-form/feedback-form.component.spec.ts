import {
  async,
  ComponentFixture,
  TestBed,
  fakeAsync,
  flush,
  tick,
} from '@angular/core/testing';
import { Type } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogRef } from '@angular/material';
import { FeedbackService } from '../feedback.service';
import { FakeFeedbackService } from '../testing/fake-feedback.service';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FeedbackFormComponent } from './feedback-form.component';
import { SharedModule } from '../../shared/shared.module';
import { Store } from '@ngrx/store';

describe('FeedbackFormComponent', () => {
  let component: FeedbackFormComponent;
  let fixture: ComponentFixture<FeedbackFormComponent>;
  let feedbackSrvc: FeedbackService;
  let dialogRef: MatDialogRef<FeedbackFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        NoopAnimationsModule,
        SharedModule,
      ],
      declarations: [FeedbackFormComponent],
      providers: [
        {
          provide: FeedbackService,
          useClass: FakeFeedbackService,
        },
        {
          provide: MatDialogRef,
          useValue: {
            close: () => null,
          },
        },
        {
          provide: Store,
          useValue: {
            dispatch: () => null,
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedbackFormComponent);
    component = fixture.componentInstance;
    feedbackSrvc = fixture.debugElement.injector.get<FeedbackService>(
      FeedbackService as Type<FeedbackService>
    );
    dialogRef = fixture.debugElement.injector.get<
      MatDialogRef<FeedbackFormComponent>
    >(MatDialogRef as Type<MatDialogRef<FeedbackFormComponent>>);

    fixture.detectChanges();
  });

  it('should create component and intiliaze state', () => {
    expect(component).toBeTruthy();
    expect(component.submitted).toBe(false);
    expect(component.isSaving).toBe(false);
  });

  it('should only submit a valid form, toggle the saving and form submitted states', fakeAsync(() => {
    const spy = spyOn(feedbackSrvc, 'postFeedback').and.returnValue(
      of(null).pipe(delay(300))
    );

    expect(component.feedbackForm.valid).toBe(false);

    component.submit();
    expect(spy).not.toHaveBeenCalled();

    component.feedbackForm.patchValue({
      text: 'test feedback',
    });

    expect(component.feedbackForm.valid).toBe(true);

    component.submit();

    expect(component.isSaving).toBe(true);

    expect(feedbackSrvc.postFeedback).toHaveBeenCalledWith({
      text: 'test feedback',
      anonymous: false,
      rating: null,
      device: null,
    });

    tick(400);

    expect(component.isSaving).toBe(false);
    expect(component.submitted).toBe(true);

    flush();
  }));
});
