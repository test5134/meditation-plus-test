import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { FeedbackFormComponent } from './feedback-form/feedback-form.component';
import { FeedbackFormDirective } from './open-feedback-form.directive';

@NgModule({
  declarations: [FeedbackFormComponent, FeedbackFormDirective],
  entryComponents: [FeedbackFormComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  exports: [FeedbackFormDirective],
})
export class FeedbackModule {}
