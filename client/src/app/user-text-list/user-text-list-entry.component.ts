import {
  Component,
  Input,
  ChangeDetectionStrategy,
  OnInit
} from '@angular/core';

@Component({
  selector: 'user-text-list-entry',
  templateUrl: './user-text-list-entry.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: [
    './user-text-list-entry.component.styl'
  ]
})
export class UserTextListEntryComponent implements OnInit {
  @Input() user;
  @Input() date;
  @Input() text;
  @Input() timeAgo = true;
  @Input() edited: boolean;

  isRecognizedUser: boolean;

  ngOnInit() {
    this.isRecognizedUser = !!(this.user && (this.user.name !== 'Deleted User' && this.user.name !== 'Anonymous'));
  }
}
