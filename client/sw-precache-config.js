module.exports = {
  navigateFallback: '/index.html',
  stripPrefix: 'dist',
  root: 'dist/',
  maximumFileSizeToCacheInBytes: 5242880, // 5 MiB
  staticFileGlobs: [
    'dist/index.html',
    'dist/**.js',
    'dist/**.css',
    'dist/assets/**.*'
  ],
  importScripts: ['firebase-messaging-sw.js']
};
