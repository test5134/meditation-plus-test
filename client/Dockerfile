
# Stage 1 (Build Angular Application)
FROM node:14-alpine as builder

ARG GITREF="master"

COPY package.json yarn.lock ./

RUN yarn --frozen-lockfile --link-duplicates && mkdir /app && mv ./node_modules ./app

WORKDIR /app

COPY . .

RUN yarn build --prod && yarn sw && mv /app/dist/service-worker.js /app/dist/sw.js
RUN echo "{ \"version\": \"${GITREF}\" }" > /app/dist/assets/version.json

# Stage 2 (nginx)
FROM nginx:1.18-alpine as runtime

# Remove default nginx index page
RUN rm -rf /usr/share/nginx/html/*

# Copy from the stage 1
COPY --from=builder /app/dist /usr/share/nginx/html

COPY ./etc/nginx.conf /etc/nginx/nginx.conf

RUN addgroup -S medplus && adduser -S -G medplus medplus
RUN mkdir -p /var/run
RUN chgrp -R medplus /var/cache/nginx /var/run /var/log/nginx /usr/share/nginx/html && \
    chmod -R 770 /var/cache/nginx /var/run /var/log/nginx /usr/share/nginx/html

USER medplus

EXPOSE 8080

ENTRYPOINT ["nginx", "-g", "daemon off;"]
